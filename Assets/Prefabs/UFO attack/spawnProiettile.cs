﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnProiettile : MonoBehaviour
{
    public Rigidbody2D proiettile;
    public int speed = 50;
    public float fireRate = 0.5F;
    private float nextFire = .5F;
    public GameObject canvasPadre;

    public Transform cursor;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextFire)
        {
            Rigidbody2D instantiatedbullet = Instantiate(proiettile, transform.position, transform.rotation) as Rigidbody2D;
            instantiatedbullet.transform.SetParent(canvasPadre.transform);
            
            nextFire = Time.time + fireRate;

            Vector3 direction = (transform.position - cursor.position).normalized;

            instantiatedbullet.transform.rotation = Quaternion.LookRotation(direction) * Quaternion.Euler(new Vector3(0,90,113));
            instantiatedbullet.velocity = -direction * speed;
        };
    }
}