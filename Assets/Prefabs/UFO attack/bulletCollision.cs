﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletCollision : MonoBehaviour
{    
    public float range = 1;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 4);
    }

    // Update is called once per frame
    void Update()
    {   
        float distanceToEnemy = Vector3.Distance(transform.position, VirtualCursor.instance.transform.position);
        if (distanceToEnemy < range)
        {
            Destroy(gameObject);
        }
    }
}
