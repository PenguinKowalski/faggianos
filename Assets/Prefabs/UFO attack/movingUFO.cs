﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingUFO : MonoBehaviour
{
    private int startSpeed = 20;
    public int speed = 20;
    public float range = 10f;
    public Transform target;
    public Rigidbody2D UFO;

    //use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = (transform.position - target.transform.position).normalized;
        UFO.velocity = -direction * speed;

        float shortestDistance = Mathf.Infinity;
        Transform cursoreVicino = null;


        float distanceToEnemy = Vector3.Distance(transform.position, target.transform.position);
        if (distanceToEnemy < shortestDistance)
        {
            shortestDistance = distanceToEnemy;
            cursoreVicino = (target);
        }

        if (cursoreVicino != null && shortestDistance <= range)
        {
            target = cursoreVicino;
            {
                speed = 0;
            }
        }
        if (cursoreVicino != null && shortestDistance > range)
        {
            target = cursoreVicino;
            {
                speed = startSpeed;
            }
        }
    }
}
