﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class TanksMove : Minigame
{
    public Transform tank1, tank2, tank3;

    public GameObject winButton, loseButton;

    private Vector2 pos1tank1 = new Vector2(30, 0);
    private Vector2 pos2tank1 = new Vector2(-600, 0);
    private Vector2 pos1tank2 = new Vector2(-10, 0);
    private Vector2 pos2tank2 = new Vector2(600, 0);
    private Vector2 pos1tank3 = new Vector2(-150, 0);
    private Vector2 pos2tank3 = new Vector2(300, 0);

    public float speed = 0.1f;

    private Vector3 startpos1;
    private Vector3 startpos2;
    private Vector3 startpos3;

    public RectTransform pointer;
    private Vector3 mousePosition;

    public Transform barrel;

    float verticalSpeed = 2.0f;

    public float zRotation = 0f;

    public float mouseSens = 50f;

    private GameObject target;

    void Start()
    {
        startpos1 = tank1.localPosition;
        startpos2 = tank2.localPosition;
        startpos3 = tank3.localPosition;

        target = GameObject.FindGameObjectWithTag("GameController");

        //VirtualCursor.instance.transform.position = Input.mousePosition;
    }

    void Update()
    {
        if (tank1!= null)
        {
            tank1.localPosition = Vector3.Lerp(pos1tank1, pos2tank1, Mathf.PingPong(Time.time * speed, 1.0f)) + startpos1;
        }
        if (tank2 != null)
        {
            tank2.localPosition = Vector3.Lerp(pos1tank2, pos2tank2, Mathf.PingPong(Time.time * speed, 1.0f)) + startpos2;
        }
        if (tank3 != null)
        {
            tank3.localPosition = Vector3.Lerp(pos1tank3, pos2tank3, Mathf.PingPong(Time.time * speed, 1.0f)) + startpos3;
        }

        //pointer.position = Camera.main.ScreenToWorldPoint(new Vector3(VirtualCursor.instance.transform.position.x, VirtualCursor.instance.transform.position.y, 0));
        pointer.transform.position = VirtualCursor.instance.transform.position;

        float v = verticalSpeed * Input.GetAxis("Mouse X") * mouseSens * Time.fixedDeltaTime;

        Vector3 direction = pointer.position - barrel.position;

        barrel.right = direction;

        barrel.localRotation = barrel.localRotation * Quaternion.Euler(0, 0, 90);

        if(tank1 == null && tank2== null && tank3 == null)
        {
            winButton.SetActive(true);
            loseButton.SetActive(true);
        }
    }

    public void youWin()
    {
        Victory();
    }

    public void youLose()
    {
        Lose();
    }
}

    