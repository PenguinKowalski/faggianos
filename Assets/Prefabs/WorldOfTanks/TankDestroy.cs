﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankDestroy : MonoBehaviour
{
    public float distance;
    public Transform pointer;
    void Update()
    {
        Vector3 a, b;
     
        a = new Vector3(this.transform.position.x, this.transform.position.y, 0);
        b = new Vector3(pointer.position.x, pointer.position.y, 0);
        float dis = Vector3.Distance(a, b);

        if (Input.GetMouseButtonDown(0) && dis< distance)
            Destroy(this.gameObject);
    }
}
