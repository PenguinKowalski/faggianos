﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spider : MonoBehaviour
{
    public GameObject target;
    public float range = 50f;
    
    public GameObject prefabRagnatela;
    public GameObject parent;
    bool spawnOneTime;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float shortestDistance = Mathf.Infinity;
        GameObject cursoreVicino = null;

        
        float distanceToEnemy = Vector3.Distance(transform.position, target.transform.position);
        if (distanceToEnemy < shortestDistance)
        {
            shortestDistance = distanceToEnemy;
            cursoreVicino = target;
        }
        
        if (cursoreVicino != null && shortestDistance <= range)
        {
            target = cursoreVicino;
            
            if (!spawnOneTime)
            {
                GameObject ragnatela = Instantiate(prefabRagnatela, VirtualCursor.instance.transform.position + offset, transform.rotation, VirtualCursor.instance.transform) as GameObject;
              //  ragnatela.transform.SetParent(VirtualCursor.instance.transform);
                VirtualCursor.instance.Sensitivity = 0.2f;
                StartCoroutine(ActivationRoutine(ragnatela));
            }
            spawnOneTime = true;            
        }
        if( cursoreVicino != null && shortestDistance > range)
        {
            spawnOneTime = false;
        }
    }

    private IEnumerator ActivationRoutine(Object ragnatela)
    {
        //Wait for 5 secs.
        yield return new WaitForSeconds(5f);
        Destroy(ragnatela);
        VirtualCursor.instance.Sensitivity = VirtualCursor.instance.StartSensitivity;
    }
}
