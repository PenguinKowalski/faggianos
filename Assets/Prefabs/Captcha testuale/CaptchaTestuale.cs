﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using JetBrains.Annotations;

public class CaptchaTestuale : MonoBehaviour
{
    public GameObject canvas;
    public TextMeshProUGUI[] text;

    TextMeshProUGUI tmp;

    private AudioSource audioFagiano;

    public GameObject textPrefab;

    public TMP_InputField inputField;

    float currentTime = 0f;
    float startTime = 5f;
    [SerializeField] TextMeshProUGUI countdownText;

    // Start is called before the first frame update
    void Start()
    {
        GameObject canvas = GameObject.Find("Base Image");

        audioFagiano = GetComponent<AudioSource>();

        currentTime = startTime;
    }

    // Update is called once per frame
    public void OnButtonClick()
    {
        countdownText.gameObject.SetActive(true);

        currentTime = startTime;
        countdownText.color = Color.white;


        if (tmp != null)
        {
            GameObject.Destroy(tmp.gameObject);
        }

        TextMeshProUGUI InstantiatedText = Instantiate(text[Random.Range(0, text.Length)], transform.localPosition + new Vector3(Random.Range(-250f, 250f), Random.Range(25f, 80f), 0), Quaternion.Euler(0, 0, Random.Range(-30, 30)));

        tmp = InstantiatedText;

        InstantiatedText.transform.SetParent(canvas.transform, false);
    }

    public void audioPress()
    {
        audioFagiano.Play();
    }

    public void infoPress()
    {
        textPrefab.SetActive(true);
        StartCoroutine(ActivationRoutine());
    }

    private IEnumerator ActivationRoutine()
    {
        //Wait for 1.5 secs.
        yield return new WaitForSeconds(1.5f);
        textPrefab.SetActive(false);
    }

    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString("0");


        if (currentTime <= 0)
        {
            countdownText.color = Color.red;
            currentTime = 0;
        }

        tmp.text = "fagiano";
        if (inputField.text == "fa")
        {
            tmp.text = "fagiani";
        }
        if (inputField.text == "fag")
        {
            tmp.text = "fagiani";
        }
        if (inputField.text == "fagi")
        {
            tmp.text = "fagihini";
        }
        if (inputField.text == "fagia")
        {
            tmp.text = "zagihini";
        }
        if (inputField.text == "fagian")
        {
            tmp.text = "zacchini";
        }
        if (inputField.text == "fagiano")
        {
            tmp.text = "zucchini";
            Debug.Log("hai vinto,forse");
        }
    }
}