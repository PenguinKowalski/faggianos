﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceBallRainbow : MonoBehaviour
{
    //Give it a momentary force of 5 in the X direction but you can change this in unity to apply
    //an impulse force in any direction.
    public Vector2 impulseMagnitude = new Vector2(5.0f, 0.0f);

    bool m_oneTime = true;

    Rigidbody2D rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_oneTime)
        {
            rb.AddForce(impulseMagnitude, ForceMode2D.Impulse);
            m_oneTime = false;
           // Debug.Log("Here");
        }
    }
}
