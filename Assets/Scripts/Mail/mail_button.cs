﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class mail_button : MonoBehaviour
{
    public Mail data_mail;
    public TextMeshProUGUI _from, _object;

    public outlook_manager OM;
    public bool isSent;

    private void Start()
    {
        OM = this.GetComponentInParent<outlook_manager>();
    }
    public void open_text_OnClick(GameObject _mail)
    {       

        //mail_button button_arrived = _mail.GetComponent<mail_button>();

        if (!isSent)
            OM.OpenMail(data_mail);
        else
            OM.OpenSentMail(data_mail);


    }
}
