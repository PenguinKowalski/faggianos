﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Mail", menuName = "Mail/MailAsset", order = 1)]
public class Mail : ScriptableObject
{
    [Tooltip("devono essere valori UNICI")]
    [SerializeField] string cod;
    [SerializeField] string messaggio;
    [SerializeField] string[] risposte;
    [SerializeField] string[] eventiRisposta;

    [SerializeField] string indirizzo;
  //  [SerializeField] string allegato;
    [SerializeField] string oggetto;

    [SerializeField] bool isMinigameAllegato;
   
    [Tooltip("-1 è randomico, gli altri sono gli indici della lista minigiochi")]
    [SerializeField] int minigameAllegato=-1;

    [SerializeField] DateTime tempoRicezione;

    [SerializeField] Mail[] nextMail;

    [SerializeField] int maxSpawns=1;

    [SerializeField] int mailruntype = -1;

    [SerializeField] bool firstMail=false;



    public string Messaggio { get => messaggio; set => messaggio = value; }
    public string Indirizzo { get => indirizzo; set => indirizzo = value; }
   // public string Allegato { get => allegato; set => allegato = value; }
    public string Oggetto { get => oggetto; set => oggetto = value; }
    public string Cod { get => cod; set => cod = value; }
  //  public bool IsRead { get => isRead; set => isRead = value; }
    public bool IsMinigameAllegato { get => isMinigameAllegato; set => isMinigameAllegato = value; }
    public int MinigameAllegato { get => minigameAllegato; set => minigameAllegato = value; }
    public int MaxSpawns { get => maxSpawns; set => maxSpawns = value; }
    public string[] Risposta { get => risposte; set => risposte = value; }
    public string[] EventiRisposta { get => eventiRisposta; set => eventiRisposta = value; }
    public bool FirstMail { get => firstMail; set => firstMail = value; }
    public Mail[] NextMail { get => nextMail; set => nextMail = value; }

    void FollowLink()
    {

    }
    void MostraRisposta()
    {
        
    }

    void IniziaRisposta()
    {

    }
}


public class MailLocalRunData
{
    public int MaxSpawns;
    public bool isRead;
    public int selectAnswerIndex=0;

    public MailLocalRunData(int maxSpawns, bool isRead)
    {
        MaxSpawns = maxSpawns;
        this.isRead = isRead;
        
    }
}