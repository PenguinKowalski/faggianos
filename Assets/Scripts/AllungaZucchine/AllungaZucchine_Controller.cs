﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllungaZucchine_Controller : Minigame
{
    public GameObject MainScreen, WinScreen;
   // public Database db_zucchine;
    public int perdita = 20;
    public void RifiutaOnClick()
    {
        GameManager.Instance.database.ChangeZucchini(-perdita);
        Lose();
    }

    public void AccettaOnClick()
    {
        MainScreen.SetActive(false);
        WinScreen.SetActive(true);
        Victory();
    }
}
