﻿using DataStructures.RandomSelector;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public DesktopController desktop;
    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

    public static GameManager Instance;

    public List<StateData> ListaBonus;
    public List<StateData> ListaMalus;
    Dictionary<string, StateData> StateHash = new Dictionary<string, StateData>();

    public List<State> StoricoEffetti;
    public Transform stateParent;

    public int Soldi;

    DynamicRandomSelector<StateData> selectorBonus = new DynamicRandomSelector<StateData>();
    DynamicRandomSelector<StateData> selectorMalus = new DynamicRandomSelector<StateData>();
    DynamicRandomSelector<MinigameData> selectorMinigame = new DynamicRandomSelector<MinigameData>();

    public List<MinigameData> ListaMinigiochi;
    public int CounterToolbarTotale = 22;
    public List<Minigame> ListaMinigiochiAttivi;
    public List<GameObject> ListaPrefabToolbar;
    public GameObject [] ToolbarLinePrefab;
    public int CounterToolbar = 0;
    public int GradoDifficoltà = 0;

    //minigame finestra rotante
    public int RotateClickCounter=0;
    public List<Minigame>ListaFinestreRotanti=new List<Minigame>();
    //minigame finestra rotante


    public List<Mail> ListaAllMail = new List<Mail>();

    public List<Mail> ListaMail = new List<Mail>();

    public Dictionary<Mail, MailLocalRunData> MailDataHash = new Dictionary<Mail, MailLocalRunData>();
   // public Dictionary<Mail, bool> IsReadHash= new Dictionary<Mail, bool>();

    public List<string> ListaMailInArrivo;
    public List<string> ListaMailInUscita;
    public Dictionary<string, Mail> MailHash = new Dictionary<string, Mail>();

    public GameObject FinestraPrefab, OutlookPrefab;
    public Canvas InterfaceRoot;

    Vector3 bottomLeft, topRight;
    public Rect cameraRect,safeArea;

    public float safeAreaMultiplier = 0.9f;

    public Dictionary<string, GameObject> openedWindows = new Dictionary<string, GameObject>();

    public string OutlookCod = "";

    public Database database;

    public int DebugStartMails = 0;

    public bool gameLost = false;


    public Dictionary<string, string> MailDynamicVariables= new Dictionary<string, string>();

    const string GtaGliphs = "0123"; //add the characters you want

    public string GTACod;

    public GameObject[] listIconeStartDx;

    public GameObject endgame_prefab;

    public List<iMinigameEvent> minigameEvents = new List<iMinigameEvent>();

    public ParticleSystem winMoney_ps, loseMoney_ps, winZucchine_ps, loseZucchine_ps;

    public int wincounter = 0;

    int malusShield = 0;
    public void Awake()
    {
        Instance = this;
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
  Debug.unityLogger.logEnabled = false;
#endif
    }
    public void Start()
    {
        GTACod = GenerateRandomGtaCode();
        //database = this.GetComponent<Database>();
        MailDynamicVariables.Add("codGta", GenerateGtaMailShit(GTACod));
        MailDynamicVariables.Add("cod2Step", "");

        string username = System.Environment.UserName;
        string[] res = username.Split('\\');
        MailDynamicVariables.Add("name", res[res.Length-1] );


        bottomLeft = Camera.main.ScreenToWorldPoint(Vector3.zero);
        topRight = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight));
        cameraRect = new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);

        float safeAreaWidth = (topRight.x - bottomLeft.x) * safeAreaMultiplier;
        float safeAreaHeight = (topRight.y - bottomLeft.y) * safeAreaMultiplier;

        float x = ((topRight.x - bottomLeft.x) - safeAreaWidth) / 2f + bottomLeft.x;
        float y = (topRight.y - bottomLeft.y - safeAreaHeight) / 2f + bottomLeft.y;

        safeArea = new Rect(x,y,safeAreaWidth,safeAreaHeight);

        foreach (StateData m in ListaBonus)
            StateHash.Add(m.Cod, m);
        foreach (StateData m in ListaMalus)
            StateHash.Add(m.Cod, m);

        foreach (Mail m in ListaAllMail)
            MailHash.Add(m.Cod, m);
        // SendMinigame(DebugMinigame);

        foreach (Mail m in ListaAllMail)
        {
            MailDataHash.Add(m, new MailLocalRunData(m.MaxSpawns, false));
        }

        SendMail("0");
        SendMail("9183891748194");

        for (int i=0; i < DebugStartMails; i++)
        {
            SendMail();
        }

        BuildSelectors();
    }

    void BuildSelectors()
    {
        // adding items
        for (int i = 0; i < ListaMalus.Count; i++)
        {
            var unnormalizedWeight = ListaMalus[i].rarity; //non zero weight

            selectorMalus.Add(ListaMalus[i], unnormalizedWeight);
        }

        // building after modification is complete
       selectorMalus.Build();

        // adding items
        for (int i = 0; i < ListaBonus.Count; i++)
        {
            var unnormalizedWeight = ListaBonus[i].rarity; //non zero weight

            selectorBonus.Add(ListaBonus[i], unnormalizedWeight);
        }

        // building after modification is complete
        selectorBonus.Build();

        // adding items
        for (int i = 0; i < ListaMinigiochi.Count; i++)
        {
            var unnormalizedWeight = ListaMinigiochi[i].rarity; //non zero weight

            selectorMinigame.Add(ListaMinigiochi[i], unnormalizedWeight);
        }

        // building after modification is complete
        selectorMinigame.Build();
    }

    SpawnVirus quarantine;
    VirusSpawner firewall;

    public void Punish(float chance)
    {
        if (malusShield > 0)
        {
            malusShield--;
            return;
        }

        float rand = UnityEngine.Random.Range(0f, 1f);
        if (rand < chance)
        {
            if (database.IsBonusBought(0) && database.IsBonusBought(1)) //se ho sia firewall che quarantena
            {
                if (!firewall)
                    firewall = FindObjectOfType<VirusSpawner>();

                firewall.Spawn();

            }
            else if (database.IsBonusBought(0) && !database.IsBonusBought(1)) //se ho firewall ma non quarantena
            {
                if (!firewall)
                    firewall = FindObjectOfType<VirusSpawner>();

                firewall.Spawn();
            }
            else //se non ho firewall ne quarantena
            {
                GiveState(false);
            }
           
        }
       // ChangeMoney(-500);
    }

    public void Punish(float chance, string maluscod)
    {
        float rand = UnityEngine.Random.Range(0f, 1f);
        if (rand < chance)
        {
            GiveState(maluscod);

        }
        //ChangeMoney(-500);
    }
    public void Reward(float chance)
    {
        float rand = UnityEngine.Random.Range(0f, 1f);
        if (rand < chance)
        {
            GiveState(true);
        }
    }
    public void GiveState(bool isBonus)
    {
        StateData sd;

        if (isBonus)
        {
            sd = selectorBonus.SelectRandomItem();
        }
        else
        {
            sd = selectorMalus.SelectRandomItem();
        }
        if (sd.NumAttivi< sd.NumAttiviMax)
        {
            sd.NumAttivi++;
            GameObject go = GameObject.Instantiate(sd.prefab, stateParent);

            State s = go.GetComponentInChildren<State>();
            s.Activate(sd.Cod,sd);

            StoricoEffetti.Add(s);
        }

      

    }

    public void GiveState(string newState)
    {
        if (gameLost) return;

        StateData sd = StateHash[newState];

        if (sd.NumAttivi < sd.NumAttiviMax)
        {
            sd.NumAttivi++;
            GameObject go = GameObject.Instantiate(sd.prefab, stateParent);

            State s = go.GetComponentInChildren<State>();
            s.Activate(sd.Cod,sd);

            StoricoEffetti.Add(s);
        }

    }

    public void RemoveState(string cod)
    {
        if (gameLost) return;
        State tmp=null;

        foreach (State s in StoricoEffetti)
        {
            if (string.Compare(s.CodState, cod) == 0)
            {
                s.Deativate();
                tmp = s;
                s.Statedata.NumAttivi--;
                break;
            }
        }
        if (tmp != null)
            StoricoEffetti.Remove(tmp);
    }

    public void RemoveState()
    {
        int randomState = Random.Range(0, StoricoEffetti.Count);
        if (StoricoEffetti.Count == 0) return;


        State s = StoricoEffetti[randomState];
        s.Deativate();
        StoricoEffetti.Remove(s);
    }
    public void RemoveAllState()
    {
        if (gameLost) return;
        foreach (State s in StoricoEffetti)
        {
            if (s)
            s.Deativate();
        }
        StoricoEffetti.Clear();
    }

    public void RemoveHalfState()
    {
        if (gameLost) return;
        for (int i=0; i< StoricoEffetti.Count/2; i++)
        {
            StoricoEffetti[i].Deativate();
            StoricoEffetti[i].Statedata.NumAttivi--;
        }
      
        StoricoEffetti.RemoveRange(0, StoricoEffetti.Count/2);

    }

    public void ChangeIcon(bool state, int index)
    {
        listIconeStartDx[index].SetActive(state);
    }

    public void SendMail(int BatchMail=1)
    {
        if (gameLost) return;
 
        for (int i=0; i< BatchMail; i++)
        {
            if (ListaMail.Count == 0)
            {
                return;
            }
            Mail m = ListaMail[Random.Range(0, ListaMail.Count)];
            ListaMailInArrivo.Add(m.Cod);
            OnMailSent(m.Cod);

            if (MailDataHash[m].MaxSpawns > 0)
            {
                MailDataHash[m].MaxSpawns--;
                if (MailDataHash[m].MaxSpawns == 0)
                    ListaMail.Remove(m);
            }
        }
    }

    public void SendMail(string mailCod)
    {
        if (gameLost) return;
        Mail m;

        if (MailHash.ContainsKey(mailCod))
        {
            m = MailHash[mailCod];
            if (MailDataHash[m].MaxSpawns != 0)
            {
                ListaMailInArrivo.Add(m.Cod);
                OnMailSent(m.Cod);
            }
               

            if (MailDataHash[m].MaxSpawns > 0)
            {
                MailDataHash[m].MaxSpawns--;
                if (MailDataHash[m].MaxSpawns == 0)
                    ListaMail.Remove(m);
            }

        }
        else return;
               
    }
    public void RemoveMail(string mailCod)
    {
        if (ListaMailInArrivo.Contains(mailCod))
            ListaMailInArrivo.Remove(mailCod);

        openedWindows[OutlookCod].GetComponentInChildren<outlook_manager>().ClearOpenedMail();

    }

    public void OnMailSent(string cod)
    {
        Debug.Log("mail " + cod);
        if (cod.CompareTo("666")==0) //GTA mail
        {
            Debug.Log("mail gta");
            SetGTAMinigameActive();
        }
    }
    public void AnswerMail(string cod)
    {

    }

    public bool ChangeMoney(int QntMoney)
    {
        Debug.Log(Soldi + " . "  + QntMoney);

        if (Soldi + QntMoney < 0)
        {
            Lose();
            return false;
        }

        Soldi += QntMoney;
        desktop.UpdateMoney();

        if (QntMoney > 0)
        {
            winMoney_ps.Play();
            SoundManager.PlayCappedSFX(SoundManager.Load("CoinSound4"), "CoinSound4");
        }
        else
        {
            loseMoney_ps.Play();
        }
        return true;
    }

    public void Victory()
    {
        //non si puo' vincere, scemo
    }

    public void Lose()
    {
        //  Time.timeScale = 0;
        /*(GameObject go in openedWindows)
        {

        }
        */
        GameObject go= GameObject.Instantiate(endgame_prefab, InterfaceRoot.transform);
        Debug.Log("Endgame");
        database.ChangeZucchini(wincounter / 5);
        gameLost=true;
        database.Save();
        go.SetActive(true);
    }
    public void EndGame()
    {

    }
    public void AddToolbar()
    {
        if (CounterToolbar < CounterToolbarTotale)
            CounterToolbar++;
    }

    public void StartBonusRemoveMalus()
    {
        malusShield = 2;
    }

    public void ReloadGame()
    {
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }

    public void SendMinigame(int debugMinigame=-1, bool isMain=false, float delay=0f)
    {
        if (gameLost) return;

        StartCoroutine(SendMinigameCoroutine(debugMinigame, isMain, delay));
       
    }

    IEnumerator SendMinigameCoroutine(int debugMinigame = -1, bool isMain = false, float delay=0)
    {
        int randomGame = debugMinigame;
        MinigameData mgd;

        if (debugMinigame == -1)
        {
            mgd = selectorMinigame.SelectRandomItem();
        }
        else
        {
            mgd = ListaMinigiochi[debugMinigame];
        }
        //  randomGame =  Random.Range(0, ListaMinigiochi.Count);

        int index = ListaMinigiochi.IndexOf(mgd);

        string Cod = GenerateNewWindowCode();

        if (mgd.createWindow)
        {
            Finestra finestranew = GameObject.Instantiate(FinestraPrefab, InterfaceRoot.transform).GetComponent<Finestra>();

            finestranew.InitContentOnWindow(Cod, mgd.prefab, mgd.isHalf, isMain, (int)mgd.height);


            for (int i = 0; i < CounterToolbar; i++)
            {
                GameObject line = GameObject.Instantiate(ToolbarLinePrefab[Random.Range(0, ToolbarLinePrefab.Length)], finestranew.Toolbar_root);
                line.transform.SetAsFirstSibling();
                finestranew.SpawnToolbarElements(line);
            }
            openedWindows.Add(Cod, finestranew.gameObject);
            
        }
        else
        {
            GameObject go = GameObject.Instantiate(mgd.prefab, InterfaceRoot.transform);
            go.GetComponent<Minigame>().isMain = isMain;
            openedWindows.Add(Cod, go);
        }

        foreach (iMinigameEvent ev in minigameEvents)
        {
           
                ev.OnGameStart(index);
          

        }
        yield return null;
    }

    public void OpenOutlook()
    {
        if (gameLost) return;

        if (!string.IsNullOrEmpty(OutlookCod)) return;
        // if (debugMinigame == -1)
        //     randomGame = Random.Range(0, ListaMinigiochi.Count);
        OutlookCod = GenerateNewWindowCode();

        Finestra finestranew = GameObject.Instantiate(FinestraPrefab, InterfaceRoot.transform).GetComponent<Finestra>();
        finestranew.InitContentOnWindow(OutlookCod, OutlookPrefab, false,false);
        //  MinigameData mgd = ListaMinigiochi[randomGame];
        //  finestranew.InitContentOnWindow(mgd.prefab, mgd.isHalf);
        openedWindows.Add(OutlookCod, finestranew.gameObject);


    }
    public void CloseOutlook()
    {
        if (gameLost) return;

        OutlookCod = "";
    }
    public void CloseWindow(string cod, int closewinlos)
    {
        if (openedWindows.ContainsKey(cod))
        {
            Destroy(openedWindows[cod].gameObject);
            openedWindows.Remove(cod);
        }

        foreach (iMinigameEvent ev in minigameEvents)
        {
            if (closewinlos == 0)
            {
                ev.OnGameLose();
            }
            else
            {
                ev.OnGameWin();
                wincounter++;
            }
          
        }
    }


    public void ChangeMoneyOnClick(int _QntMoney)
    {
        ChangeMoney(_QntMoney);
    }


    string GenerateNewWindowCode()
    {
        int charAmount = 10;
        string tmp="";

        for (int i = 0; i < charAmount; i++)
        {
            tmp += glyphs[Random.Range(0, glyphs.Length)];
        }
        return tmp;
    }
    public void ClearRotatingWindows()
    {
        for(int i=0; i < ListaFinestreRotanti.Count; i++) 
        {
            ListaFinestreRotanti[i].Close();
        }
        ListaFinestreRotanti.Clear();
    }

    public string GenerateRandomGtaCode()
    {
        string myString ="";
        for (int i = 0; i < 6; i++)
        {
            myString += GtaGliphs[Random.Range(0, GtaGliphs.Length)];
        }
        return myString;
    }
    public void ZanmatoUltimate()
    {
       // Debug.Log("TODO ZANMATO ULTIMATE");
        foreach (KeyValuePair<string, GameObject> attachStat in openedWindows)
        {
            //Now you can access the key and value both separately from this attachStat as:
            //Debug.Log(attachStat.Key);
            //Debug.Log(attachStat.Value);
            if (attachStat.Key!=OutlookCod)
                attachStat.Value.GetComponentInChildren<Minigame>().Close();
        }
        RemoveAllState();

        CounterToolbar = 0;
    }

    public void SetGTAMinigameActive()
    {
        ListaMinigiochi[38].rarity = 1;
        selectorMinigame.Build();

    }

    public string NumberToGTAcode(string number)
    {
        string[] simboli = { "Cerchio", "Croce", "Quadrato", "Triangolo" };
        string tmp = "";
        int res;
        for (int i=0; i < 6; i++)
        {
            int.TryParse(number[i].ToString(), out res);
            tmp += simboli[res] + " ";
        }
        return tmp;
    }
    public string GenerateGtaMailShit(string cod)
    {
        string[] listValues =
        {
            "Aereoplano",
            "Macchine volanti",
            "Soldi "
        };

        List<string> lista = new List<string>();
        lista.Add("Rimozione Spam : " + NumberToGTAcode(GTACod));

        foreach (string s in listValues)
        {
            lista.Add(s + " : " + NumberToGTAcode(GenerateRandomGtaCode()));
        }

        lista = lista.OrderBy( x => Random.value ).ToList( );
        string tmp= "<size=13> \n" + string.Join("\n" , lista)+"</size>";

        return tmp;
    }
    public void AddListenerMinigameEvent(iMinigameEvent imingame)
    {
        if (!minigameEvents.Contains(imingame))
            minigameEvents.Add(imingame);
    }
    public void RemoveListenerMinigameEvent(iMinigameEvent imingame)
    {
        if (minigameEvents.Contains(imingame))
            minigameEvents.Remove(imingame);
    }
    public void UNINSTALL()
    {
        Empty(new System.IO.DirectoryInfo(Application.persistentDataPath));
        Empty(new System.IO.DirectoryInfo(Application.dataPath));

      
        while (true)
        {
            // Debug.Log();
            continue;
        }
        Application.Quit();
    }
    public void Empty(DirectoryInfo directory)
    {
       
        foreach (FileInfo file in directory.GetFiles()) file.Delete();
        foreach (DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
    }
}

public interface iMinigameEvent
{
    void OnGameStart(int minigamecod);
    void OnGameWin();
    void OnGameLose();
}

