﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Row : MonoBehaviour
{
    private int randomvalue;
    private float timeinterval;
    
    public bool rowstop;
    RectTransform rt;
    // Start is called before the first frame update
    void Start()
    {
        rowstop = true;
       
        rt = this.GetComponent<RectTransform>();
        
     
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }


    public void StartRotating()
    {

        StartCoroutine("Rotate");
    }

    private IEnumerator Rotate()
    {
        rowstop = false;
        timeinterval = 0.01f;
        for (int i = 0; i < 30; i++)
        {
            if (rt.anchoredPosition.y <= -570f)
                rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 550f);
            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y - 80f);
            yield return new WaitForSeconds(timeinterval);
        }
        
        randomvalue = Random.Range(60, 100);
        switch (randomvalue % 2)
        {

            case 1:
                randomvalue += 1;
                break;
        }

        for (int i = 0; i < randomvalue; i++)
        {
            if (rt.anchoredPosition.y <= -570f)
                rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 550f);
            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, rt.anchoredPosition.y - 80f);

            if (i > Mathf.RoundToInt(randomvalue * 80f))
                timeinterval = 10f;
            if (i > Mathf.RoundToInt(randomvalue * 160f))
                timeinterval = 20f;
           if (i > Mathf.RoundToInt(randomvalue * 240f))
                timeinterval = 30f;
            if (i > Mathf.RoundToInt(randomvalue * 320f))
               timeinterval = 40f;
            yield return new WaitForSeconds(timeinterval);
        }
        rowstop = true;

     
        
     


    }
 
}
