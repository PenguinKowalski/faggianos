﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Lever : MonoBehaviour
{
    Animator anim;
    public Row row1, row2, row3;
    public SlotManager slotmanager;
    public int soldipersi = -10;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartSlot()
    {
        anim.SetTrigger("IsClick");
        row1.StartRotating();
        row2.StartRotating();
        row3.StartRotating();

        GameManager.Instance.ChangeMoney(soldipersi);

     
    }
}
