﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropSlot : MonoBehaviour
{
    bool isDragging = false;
    Transform cursore;
    Vector3 startpos;
    RectTransform rt;
    public SlotManager slotmanager;
    public int numerocolonna;
    public string risultato;

    // Start is called before the first frame update
    void Start()
    {
        cursore=VirtualCursor.instance.gameObject.transform;
        rt = this.GetComponent<RectTransform>();
        OnPointerUp();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDragging)
            this.transform.position=new Vector3(this.transform.position.x,cursore.position.y + startpos.y,this.transform.position.z);
        if (rt.anchoredPosition.y <= -570f)
            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 550f);
        else if (rt.anchoredPosition.y >= 570f)
            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, -590f);
    }

    public void OnPointerDown()
    {
        isDragging = true;
        startpos = this.transform.position;
        
    }

    public void OnPointerUp()
    {
        isDragging = false;
        
        if (-510f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= -570f)
            risultato = "diamante";
        if (-333f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= -420f)
            risultato = "corona";
        if (-192f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= 285f)
            risultato = "melone";
        if (-30f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= -125f)
            risultato = "bar";
        if (0f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= 140f)
            risultato = "7";
        if (160f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= 300f)
            risultato = "cherry";
        if (330f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= 470f)
            risultato = "limone";
        if (490f <= rt.anchoredPosition.y && rt.anchoredPosition.y <= 550f)
            risultato = "diamante";

        if (numerocolonna == 0)
            slotmanager.ris1 = risultato;
        else if (numerocolonna == 1)
            slotmanager.ris2 = risultato;
        else if (numerocolonna == 2)
            slotmanager.ris3 = risultato;
        slotmanager.Risultato();
        Debug.Log(risultato);
    }
}
