﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    // Start is called before the first frame update
    bool flag = false;
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(5);

        flag = true;
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.anyKeyDown && flag)
        {
            GameManager.Instance.ReloadGame();
        }
    }
}
