﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace WoT
{
    public class WoTEnemy : MonoBehaviour
    {
        [SerializeField]
        public RectTransform EnemyPos,TravelEndpos,TravelStartPos;
        private PointManager PointManager;
        Vector3 target;

     //   private Vector3 SpawnPointL, SpawnPointR;
        public Sprite FlippedTank;
        public float speed, HitDistance;
        public GameObject Expuroshion,Crosschair;
        public bool IsButton;
        private Vector3 Direction;

        public void SetDirection(int r)
        {
            if (r == 0)
            {
                Direction = Vector3.right;
                EnemyPos.localScale = new Vector3(-1, 1, 1);              
            }
            else
            {
                Direction = Vector3.left;
                EnemyPos.localScale = new Vector3(1, 1, 1);
            }
        }
        void Awake()
        {
            
            EnemyPos = this.GetComponent<RectTransform>();
            Crosschair = FindObjectOfType<Crosschair>().gameObject;
            PointManager = FindObjectOfType<PointManager>();
            Expuroshion = EnemyPos.transform.GetChild(0).gameObject;
            
        }

        
        void Update()
        {

               


          
                EnemyPos.position += Direction * 1/speed;
                if (Mathf.Abs(EnemyPos.localPosition.x - TravelEndpos.transform.localPosition.x) < 1f)
                {
                    //Debug.Log("Despawn");
                    Destroy(this.gameObject);

                }
                if ( Vector3.Distance(Crosschair.transform.position , this.transform.position)< HitDistance)
                {
                 if (Input.GetButtonDown("Jump") && PointManager.Ammo > 0) 
                 Explode(); 
                }
            
            

        }

        
        public void Explode() 
        {
            PointManager.ScoreHit();
            StartCoroutine(ExplodeCo());
        }

        public IEnumerator ExplodeCo() 
        {
            Expuroshion.SetActive(true);
            yield return new WaitForSeconds(0.25f);
            //Debug.Log("BOOM!");
            Destroy(this.gameObject);
        }
    }
}
