﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WoT
{
    public class Crosschair : MonoBehaviour
    {
        private RectTransform CrossRT;
        private Vector3 Direction;
        private float H, V;
        public float Cooldown;
        public GameObject Target;        
        public Image CrossImg;
        private PointManager PointManager;
        public float ReloadTime;
       
        private void Start()
        {
            CrossRT = GetComponent<RectTransform>();
            PointManager = FindObjectOfType<PointManager>();
            CrossImg = this.GetComponent<Image>();
            Cooldown = 0;
        }

        private void Update()
        {
            Cooldown -= Time.deltaTime;
            V = Input.GetAxis("Vertical");
             H = Input.GetAxis("Horizontal");
            Direction = new Vector3(H, -V, 0);
            CrossRT.localPosition += Direction;
            if (Input.GetButtonDown("Jump")&& PointManager.Ammo>0&& Cooldown<=0)
            {
                PointManager.UseAmmo();
                Cooldown = ReloadTime;
                StartCoroutine(RedBlincCo());
            }
            if (Cooldown < 0) 
            {
                CrossImg.color = Color.white;
            }
            

        }
       
        public IEnumerator RedBlincCo() 
        {
            CrossImg.color = Color.red;
        yield return new WaitForSeconds(0.5f);
            CrossImg.color = Color.black;
        }



    }
}
