﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WoT
{

    public class SpawnManager : MonoBehaviour
    {
        public float WaweInterval;
        public RectTransform Spawn1, Spawn2;
        public GameObject FagianPref;
        public Transform Canvas;
        private float Timer;
        private void Start()
        {
            Timer = 0;
            SpawnFagiano();
        }
        private void Update()
        {
            Timer += Time.deltaTime;
            if(Timer>= WaweInterval) 
            {
                SpawnFagiano();
                //Debug.Log("SpawnFagiano");
                Timer = 0;
            }
        }

        public void SpawnFagiano() 
        {
            int r = Random.Range(0,2);
            RectTransform SelectedSpawn = (r == 0) ? Spawn1 : Spawn2;
 
            GameObject go= Instantiate(FagianPref, SelectedSpawn.position,Quaternion.identity, Canvas);
            WoTEnemy wot= go.GetComponent<WoTEnemy>();
            wot.TravelEndpos = (r == 0) ? Spawn2 : Spawn1;
            wot.TravelStartPos = SelectedSpawn;
            wot.SetDirection(r);

        }

    }
}
