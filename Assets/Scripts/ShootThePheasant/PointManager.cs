﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace WoT
{

    public class PointManager : MonoBehaviour
    {
        public int Hits, WinHit, Ammo;
        public TextMeshProUGUI AmmoText, HitText;
        public ShootThePheasant_Manager Manager;
        void Start()
        {
            Hits = 0;
            Manager = FindObjectOfType<ShootThePheasant_Manager>();

        }

        void Update()
        {
            AmmoText.text = Ammo.ToString();
            HitText.text = Hits.ToString();
            if (Hits >= WinHit)
            {
                Debug.Log("Victory");
                Manager.Victory();
            }
            if (Ammo <= 0 && Hits < WinHit)
            {
                Debug.Log("Loss");
                Manager.Lose();
            }
        }
        public void UseAmmo() { Ammo = (Ammo > 0) ? Ammo - 1 : 0; }
        public void ScoreHit() { Hits++; }


    }
}