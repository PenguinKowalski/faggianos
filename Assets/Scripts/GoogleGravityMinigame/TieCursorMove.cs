﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TieCursorMove : MonoBehaviour
{
    private Vector3 mousePos;
    private Vector3 mousePos_B;
    public float moveSpeed = 0.1f;
    public Camera CursorCam;

    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
        
            mousePos = Input.mousePosition;
        mousePos_B = CursorCam.ScreenToWorldPoint(mousePos);
        mousePos_B.z = 2f;
        transform.position = Vector3.Lerp(transform.position, mousePos_B, moveSpeed);
        Debug.Log  (mousePos_B);
        



    }
}

