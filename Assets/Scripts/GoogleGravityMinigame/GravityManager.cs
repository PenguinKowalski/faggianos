﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GravityManager : Minigame
{
    
    public Rigidbody2D rb2d;
    public float speed;
    public TextMeshPro wintext;
    public TextMeshPro losetext;
    private float moveHorizontal;
    private float moveVertical;
    public Transform GameBoardRoot, Window;
    public GameObject MainCanvas, GravRoot;
    // Start is called before the first frame update
    public override void SpawnFinestra()
    {
        GameBoardRoot.parent = null;
        
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = GravRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(GravRoot);
    }
    private void OnDestroy()
    {
        Destroy(GameBoardRoot.gameObject);
    }

    void Update()
    {

         moveHorizontal = Input.GetAxis("Horizontal");


         moveVertical = Input.GetAxis("Vertical");

    }
        // Update is called once per frame
        void FixedUpdate()
    {

      


        Vector2 movement = new Vector2(moveHorizontal, moveVertical);


        rb2d.AddForce(movement * speed);


    }

    
   
}