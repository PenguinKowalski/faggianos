﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TieMovement : MonoBehaviour
{


    public GameObject Tie;
    public GravityManager gm;

    // Start is called before the first frame update
  

    // Update is called once per frame
  

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Fallfagg"))

        {
            Debug.Log("LOSE");
            Tie.SetActive(false);
            gm.Lose();
            
           

        }

        if (collision.gameObject.CompareTag ("WinGravityButton"))

        {
            Debug.Log("WIN");
            Destroy(Tie);
            gm.Victory();
            


        }
    }
}
