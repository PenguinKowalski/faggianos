﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : Minigame
{

    public float timeLeft, buttondelay;
    public TMP_Text timelefttext, safetext;
    public Slider timeslider;
    public bool TestPassed;

    public Transform GameBoardRoot, Window;
    public GameObject MainCanvas, GameOverBaitRoot;

    public override void SpawnFinestra()
    {
        GameBoardRoot.parent = null;

        GameBoardRoot.localScale = Vector3.one;
        Window.parent = GameOverBaitRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(GameOverBaitRoot);
    }
    void Start()
    {
        TestPassed = false;
    }

    // Update is called once per frame


    void Update()
    {
        buttondelay -= Time.deltaTime;

        if (timeLeft >= 0 && buttondelay < 0)
        {
            timeLeft -= Time.deltaTime;
            timeslider.value = timeLeft;

            timelefttext.text = ("TIME LEFT = " + timeLeft.ToString("F0"));
        }

        if (timeLeft < 0)
        {
            TestPassed = true;
            safetext.text = ("GOOD JOB BRAH, YOU PASSED THE TEST!");
           
        }
    }

    public void TestCheck()

    {

        if (TestPassed==false)
        {
            Debug.Log("Minigame Lose");
            
            Lose();
        }

        if (TestPassed == true)
        {
            Debug.Log("Minigame victory");
            Victory();

        }
    }

    
}
