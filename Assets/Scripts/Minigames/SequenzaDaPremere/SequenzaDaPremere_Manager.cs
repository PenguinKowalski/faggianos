﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SequenzaDaPremere_Manager : Minigame
{
    public List <GameObject> fagiani;
    public int i;

    void Start()
    {
        i = Random.Range(0, fagiani.Count);
        fagiani[i].SetActive(true);
    }

    public void OnClick(GameObject obj)
    {
        fagiani.Remove(obj);
        Destroy(obj);

        if (fagiani.Count > 0)
        {
            i = Random.Range(0, fagiani.Count);
            fagiani[i].SetActive(true);
        }
        else
            Victory();
    }
}
