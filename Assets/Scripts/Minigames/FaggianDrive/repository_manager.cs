﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class repository_manager : MonoBehaviour
{
    public Image image_rep;
    public TextMeshProUGUI name_rep;
    public static repository_manager instance;

    string _name;
    int i;
    public int index_rep;

    private void Awake()
    {
        instance = this;
    }
    public void initializeNameRep(string name, int _index)
    {
        _name = name;
        i = _index;
        name_rep.text = _name;
        index_rep = i;
    }
}
