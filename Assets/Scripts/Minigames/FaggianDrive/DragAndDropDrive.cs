﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

public class DragAndDropDrive : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{


    //DragDrop Variables
    private Canvas canvas;
    private FaggianDrive_Manager photo_controller;

    //public Transform correct_repository;
    private Vector3 initial_pos;
    private RectTransform rect_transform;
    public GameObject correct_repository;

    //Class Variable
    public Image immagine;
    Sprite texture;

    public int indice_immagine;
    int indice;

    private void Awake()
    {


        canvas = GetComponentInParent<Canvas>();
        rect_transform = GetComponent<RectTransform>();
        photo_controller = GetComponentInParent<FaggianDrive_Manager>();
    }

    public void Update()
    {
        if (FaggianDrive_Manager.instance.counter <= 1)
            this.transform.SetParent(FaggianDrive_Manager.instance.layoutGrid);
    }

    public void Initialize(Sprite _texture, int i, GameObject corr_rep)
    {
        texture = _texture;
        indice = i;
        immagine.sprite = texture;
        indice_immagine = indice;
        correct_repository = corr_rep;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        this.transform.parent = null;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        rect_transform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      
        Debug.Log("OnPointerDown");
    }

    public void OnBeginDrag()
    {
        Debug.Log("begin drag");
        initial_pos = transform.position;
        this.transform.parent = VirtualCursor.instance.transform;

    }
    public void OnEndDrag()
    {
        Debug.Log("end drag");
        if (Mathf.Abs(transform.position.x - correct_repository.transform.position.x) <= 1f &&
            Mathf.Abs(transform.position.y - correct_repository.transform.position.y) <= 1f)
        {
          this.gameObject.SetActive(false);
          photo_controller.photos--;
        }
        else
        {
          this.transform.parent = FaggianDrive_Manager.instance.layoutGrid;
          //timer scende di X secondi
        }
    }
    public void OnMiddleDrag()
    {
        
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
