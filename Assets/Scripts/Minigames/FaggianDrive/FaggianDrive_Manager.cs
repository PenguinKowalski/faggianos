﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[System.Serializable]
public class Category
{
   public List<Sprite> sprite;
}
public class FaggianDrive_Manager : Minigame
{
    public List<string> Categorie;

    [SerializeField]
    public List<Category> databaseSprite;

    public GameObject[] Rep_Control;

    public int photos, counter;
    public int CategoryToSpawn;

    public GameObject prefabImage;
    public Transform layoutGrid;

    public GameObject prefabRepository;
    public Transform horizontalLayout;

    public List<DragAndDropDrive> Immagine;

    public int index;

    public static FaggianDrive_Manager instance;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        SpawnCartelle();

        if (tempoLimite > 0)
        {
            StartCoroutine(CloseDelayedTimer());
        }
    }

    void Update()
    {
        if (photos*CategoryToSpawn == 0)
        {
            Debug.Log("YOU WIN");
            Victory();
        }
    }

    public void SpawnCartelle()
    {
        for (index = 0; index < CategoryToSpawn; index++)
        {
            GameObject rep = Instantiate(prefabRepository, horizontalLayout);
            repository_manager rp = rep.GetComponent<repository_manager>();
            rp.initializeNameRep(Categorie[index], index);
            Rep_Control[index]= rep;

            for (int i = 0; i < photos; i++)
            {
                GameObject go = Instantiate(prefabImage, layoutGrid);
                DragAndDropDrive cp = go.GetComponent<DragAndDropDrive>();
                Immagine.Add(cp);
                cp.Initialize(databaseSprite[index].sprite[Random.Range(0, databaseSprite.Count)], index, Rep_Control[index]);
            }
        }

        for (int i = 0; i <= CategoryToSpawn; i++)
        {
            horizontalLayout.GetChild(0).SetSiblingIndex(Random.Range(i + 1, horizontalLayout.childCount));
        }

        for (int i = 0; i <= photos; i++)
        {
            layoutGrid.GetChild(0).SetSiblingIndex(Random.Range(i + 1, layoutGrid.childCount));
        }
    }


    IEnumerator CloseDelayedTimer()
    {
        counter = (int)tempoLimite;

        while (counter > 0)
        {
            string minutes = Mathf.Floor(counter / 60).ToString("00");
            string seconds = (counter % 60).ToString("00");

            counter--;
            yield return new WaitForSeconds(1);
        }
        yield return null;
    }
}
