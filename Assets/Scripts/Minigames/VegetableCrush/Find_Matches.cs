﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VegetableCrush
{
    public class Find_Matches : MonoBehaviour
    {
        private Gameboard Gameboard;
        private Score_Manager ScoreMan;
        public List<GameObject> CurrentMatches = new List<GameObject>();
        private void Awake()
        {
            Gameboard = FindObjectOfType<VegetableCrush.Gameboard>();
            ScoreMan = FindObjectOfType<VegetableCrush.Score_Manager>();

        }

        public void FindAllMatches() 
        {
            StartCoroutine(FindAllMatchesCo());
        }
        private IEnumerator FindAllMatchesCo()
        {
            yield return new WaitForSeconds(0.2f);
            for (int i = 0; i < Gameboard.Larghezza; i++)
            {
                for (int j = 0; j < Gameboard.Altezza; j++)
                {
                    GameObject CurrentVerdura = Gameboard.AllVerdure[i, j];
                    if(CurrentVerdura != null) 
                    {
                        if ((i > 0 && i < Gameboard.Larghezza - 1) && !CurrentVerdura.GetComponent<Vegetable>().IsMatched)
                        {
                            GameObject VerduraSx = Gameboard.AllVerdure[i - 1, j];
                            GameObject VerduraDx = Gameboard.AllVerdure[i + 1, j];
                            if (VerduraSx != null && VerduraDx != null)
                            {
                                if (VerduraSx.tag == CurrentVerdura.tag && VerduraDx.tag == CurrentVerdura.tag)
                                {
                                    if (!CurrentMatches.Contains(VerduraSx)) 
                                    {
                                        CurrentMatches.Add(VerduraSx);
                                    }
                                    VerduraSx.GetComponent<Vegetable>().IsMatched = true;
                                    if (!CurrentMatches.Contains(VerduraDx))
                                    {
                                        CurrentMatches.Add(VerduraDx);
                                    }
                                    VerduraDx.GetComponent<Vegetable>().IsMatched = true;
                                    if (!CurrentMatches.Contains(CurrentVerdura))
                                    {
                                        CurrentMatches.Add(CurrentVerdura);
                                    }
                                    CurrentVerdura.GetComponent<Vegetable>().IsMatched = true;
                                    int ListLenght = CurrentMatches.Count;
                                    ScoreMan.UpdateScore(ListLenght);
                                }
                            }
                        }
                        if ((j > 0 && j < Gameboard.Altezza - 1) && !CurrentVerdura.GetComponent<Vegetable>().IsMatched)
                        {
                            GameObject VerduraUp = Gameboard.AllVerdure[i, j + 1];
                            GameObject VerduraDown = Gameboard.AllVerdure[i, j - 1];
                            if (VerduraUp != null && VerduraDown != null)
                            {
                                if (VerduraUp.tag == CurrentVerdura.tag && VerduraDown.tag == CurrentVerdura.tag)
                                {
                                    if (!CurrentMatches.Contains(VerduraUp))
                                    {
                                        CurrentMatches.Add(VerduraUp);
                                    }
                                    VerduraUp.GetComponent<Vegetable>().IsMatched = true;
                                    if (!CurrentMatches.Contains(VerduraDown))
                                    {
                                        CurrentMatches.Add(VerduraDown);
                                    }
                                    VerduraDown.GetComponent<Vegetable>().IsMatched = true;
                                    if (!CurrentMatches.Contains(CurrentVerdura))
                                    {
                                        CurrentMatches.Add(CurrentVerdura);
                                    }
                                    CurrentVerdura.GetComponent<Vegetable>().IsMatched = true;
                                    int ListLenght = CurrentMatches.Count;
                                    ScoreMan.UpdateScore(ListLenght);
                                }
                            }
                        }
                    }

                }

            }
        }
       
    }
}
