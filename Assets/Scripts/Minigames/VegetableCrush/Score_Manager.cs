﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace VegetableCrush
{
    public class Score_Manager : MonoBehaviour
    {
        public TextMeshProUGUI ScoreText;
        public int Score, MinScore,MaxScore;
        private int TargetScore;
        public GameObject RedBar, WinIcon;
        private RectTransform RedBarSpRen;
        private float RedBarLenght;
        private Vector2 BarSize;
        private Gameboard Gameboard;
        private GameState ActualGameState;
        public VegetableCrush_Controller controller;

        //[SerializeField] float sliderStart, sliderEnd;
        void Start()
        {
            Score = 0;
            Gameboard = FindObjectOfType<Gameboard>();
            RedBarSpRen = RedBar.GetComponent<RectTransform>();
            //sliderEnd = -1.72f;
            //RedBarSpRen.drawMode = SpriteDrawMode.Sliced;
            ActualGameState = Gameboard.ActualGameState;
            BarSize = new Vector2(0, 1);
            WinIcon.SetActive(false);
     
            if (MinScore < 1000)
            {
                MinScore = 1000;
            }
            if (MaxScore <= MinScore)
            {
                MaxScore = 2 * MinScore;
            }
            TargetScore = Random.Range(MinScore,MaxScore);


        }

       
        void Update()
        {            
            ScoreText.text = Score.ToString();
            if (Score <= TargetScore)
            {
                RedBarLenght = (Score * 255) / TargetScore;
            }
            else 
            {
                RedBarLenght = 255;
            }
            BarSize.x = RedBarLenght;
            RedBarSpRen.GetComponent<RectTransform>().sizeDelta =new Vector2(RedBarLenght, RedBarSpRen.sizeDelta.y);
            //RedBarSpRen.localPosition = new Vector3((sliderStart+(sliderEnd-sliderStart)*(Score)/TargetScore), RedBarSpRen.localPosition.y, RedBarSpRen.localPosition.z);
            

            if (Score >= TargetScore) 
            {
                ActualGameState = GameState.Wait;
                WinIcon.SetActive(true);
                VegetableCrush_Controller vg = FindObjectOfType<VegetableCrush_Controller>();
                controller.Victory();
            }
        }
        public void UpdateScore(int MatchedVerdureNo) 
        {
            int BonusScore=0;
            if (MatchedVerdureNo > 3) 
            {
                BonusScore = MatchedVerdureNo * 100;
            }
            Score += 100 * MatchedVerdureNo + BonusScore;
        }
    }
}
