﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

namespace VegetableCrush
{
    public enum GameState 
    { 
    Wait,
    Move
    }
    public class Gameboard : MonoBehaviour
    {
        public GameState ActualGameState = GameState.Move;
        private Find_Matches FindMatches;
        public int Larghezza, Altezza;       
        public GameObject CellPrefab;
        public int[,] AllCells;
        public GameObject[] Vegetables;//Array possibili verdure
        public GameObject[,] AllVerdure; // Array verdure in campo
        public Camera rootCam;
     

        void Awake()
        {
            FindMatches = FindObjectOfType<Find_Matches>();
            //Larghezza = Random.Range(5,9);
            //Altezza = Random.Range(7, 11);
            AllCells = new int[Larghezza, Altezza];
            AllVerdure = new GameObject[Larghezza, Altezza];
            StartCoroutine(BoardGen());
            //this.transform.position = new Vector2(-Larghezza / 2, - Altezza/2); //Centra Griglia
        }

        

        
        private IEnumerator BoardGen()
        {
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < Larghezza; i++)
            {
                for (int j = 0; j < Altezza; j++)
                { //Spawna celle Griglia come child gameboard
                    Vector2 CellSpawnPos = new Vector2(this.transform.position.x + i, this.transform.position.y + j);
                    var NewCell = Instantiate(CellPrefab, CellSpawnPos, Quaternion.identity);
                    NewCell.transform.parent = this.transform;
                    NewCell.name = "Cell (" + i + "," + j + ")";
                    //Spawna Verdure nelle celle e le mette nell'array
                    int VerduraIndex = Random.Range(0, Vegetables.Length);
                    int MaxIter = 0;
                    while (PreGenMatchesAt(i, j, Vegetables[VerduraIndex]) && MaxIter < 20)
                    {
                        VerduraIndex = Random.Range(0, Vegetables.Length);
                        MaxIter++;
                    }
                    MaxIter = 0;
                    GameObject Vegetable = Instantiate(Vegetables[VerduraIndex], CellSpawnPos, Quaternion.identity);
                    Vegetable.transform.parent = this.transform;
                    Vegetable.name = Vegetable.name + "(" + i + "," + j + ")";
                    AllVerdure[i, j] = Vegetable;
                }
            }


        }

        private bool PreGenMatchesAt(int column, int raw, GameObject VegetableType)
        {
            if (column > 1 && raw > 1)
            {
                if (AllVerdure[column - 1, raw].tag == VegetableType.tag && AllVerdure[column - 2, raw].tag == VegetableType.tag)
                {
                    return true;
                }
                if (AllVerdure[column, raw - 1].tag == VegetableType.tag && AllVerdure[column, raw - 2].tag == VegetableType.tag)
                {
                    return true;
                }
            } else if (column <= 1 || raw <= 1)
            {
                if (raw > 1)
                {
                    if (AllVerdure[column, raw - 1].tag == VegetableType.tag && AllVerdure[column, raw - 2].tag == VegetableType.tag)
                    {
                        return true;
                    }
                }
                if (column > 1)
                {
                    if (AllVerdure[column - 1, raw].tag == VegetableType.tag && AllVerdure[column - 2, raw].tag == VegetableType.tag)
                    {
                        return true;
                    }
                }

            }
            return false;
        }
        private void DestroyMatchesAt(int column, int row)
        {
            if (AllVerdure[column, row].GetComponent<Vegetable>().IsMatched)
            {
                FindMatches.CurrentMatches.Remove(AllVerdure[column,row]);
                Destroy(AllVerdure[column, row]);
                AllVerdure[column, row] = null;
            }
        }
        public void DestroyAllMatches()
        {
            for (int i = 0; i < Larghezza; i++)
            {
                for (int j = 0; j < Altezza; j++)
                {
                    if (AllVerdure[i, j] != null)
                    {
                        DestroyMatchesAt(i, j);
                    }
                }
            }
            StartCoroutine(MakeVegFall());
        }
        private IEnumerator MakeVegFall()
        {
            int EmptySpaces = 0;
            for (int i = 0; i < Larghezza; i++)
            {
                for (int j = 0; j < Altezza; j++)
                {
                    if (AllVerdure[i, j] == null)
                    {
                        EmptySpaces++;
                    } else if (EmptySpaces > 0)
                    {
                        AllVerdure[i, j].GetComponent<Vegetable>().Raw -= EmptySpaces;
                        AllVerdure[i, j] = null;
                    }
                }
                EmptySpaces = 0;
            }

            yield return new WaitForSeconds(0.4f);
            StartCoroutine(FillBoardCo());
        }
        private void Refill()
        {
            for (int i = 0; i < Larghezza; i++)
            {
                for (int j = 0; j < Altezza; j++)
                {
                    if (AllVerdure[i, j] == null)
                    {
                        Vector2 TmpPos = new Vector2(i, j);
                        int VerduraIndex = Random.Range(0, Vegetables.Length);
                        GameObject Verdura = Instantiate(Vegetables[VerduraIndex], TmpPos, Quaternion.identity);
                        AllVerdure[i, j] = Verdura;
                    }

                }

            }
        }
        private bool MatchesOnBoard()
        {
            for (int i = 0; i < Larghezza; i++)
            {
                for (int j = 0; j < Altezza; j++)
                {
                    if (AllVerdure[i, j] != null)
                    {
                        if (AllVerdure[i, j].GetComponent<VegetableCrush.Vegetable>().IsMatched)
                        {
                            return true;
                        }
                    }

                }

            }
            return false;
        }
        private IEnumerator FillBoardCo() 
        {
            Refill();
            yield return new WaitForSeconds(0.5f);
            int MaxIter = 0;
            while (MatchesOnBoard() && MaxIter < 50) 
            {
                yield return new WaitForSeconds(0.5f);
                DestroyAllMatches();
                MaxIter++;
            }
            MaxIter = 0;
            yield return new WaitForSeconds(0.5f);
            ActualGameState = GameState.Move;
        }
       
    }
  
}
