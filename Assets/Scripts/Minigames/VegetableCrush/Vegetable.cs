﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VegetableCrush
{
    public class Vegetable : MonoBehaviour,IVirtualClick
    {
        public int Raw, Column, PrevRaw, PrevCol;
        private GameObject OtherVegetable;
        private VegetableCrush.Gameboard Gameboard;
        private Vector2 StartPos, EndPos, TravelDestination;
        private Find_Matches FindMatches;
        public float SwipeAngle = 0f, SwipeResistence=0.25f;
        public int TargetX, TargetY;
        public bool IsMatched = false;
        
        void Start()
        {
            Gameboard = FindObjectOfType<VegetableCrush.Gameboard>();
            FindMatches = FindObjectOfType<VegetableCrush.Find_Matches>();
            TargetX = (int)transform.localPosition.x;
            TargetY = (int)transform.localPosition.y;
            Raw = TargetY;
            Column = TargetX;
            PrevRaw = Raw;
            PrevCol = Column;
        }      
        void Update()
        {
            TargetX = Column;
            TargetY = Raw;
            if (Mathf.Abs(TargetX-transform.position.x)>0.01f) 
            {
                TravelDestination = new Vector2(TargetX, transform.position.y);
                transform.position= Vector2.Lerp(transform.position, TravelDestination, Time.deltaTime*5f);
                if (Gameboard.AllVerdure[Column, Raw] != this.gameObject) 
                {
                    Gameboard.AllVerdure[Column, Raw] = this.gameObject;
                }
                FindMatches.FindAllMatches();
            } else
            {
                TravelDestination = new Vector2(TargetX, transform.position.y);
                transform.position = TravelDestination;
                
            }

            if (Mathf.Abs(TargetY - transform.position.y) > 0.01f)
            {
                TravelDestination = new Vector2(transform.position.x, TargetY);
                transform.position = Vector2.Lerp(transform.position, TravelDestination, Time.deltaTime*5f);
                if (Gameboard.AllVerdure[Column, Raw] != this.gameObject)
                {
                    Gameboard.AllVerdure[Column, Raw] = this.gameObject;
                }
                FindMatches.FindAllMatches();
            } else
            {
                TravelDestination = new Vector2(transform.position.x, TargetY);
                transform.position = TravelDestination;
               
            }
            //CheckMatch();

            if (IsMatched) 
            {
                SpriteRenderer SpriteRend = this.GetComponent<SpriteRenderer>();
                SpriteRend.color = new Color(1f, 1f, 1f, 0.2f);
            }

            if (isswipe)
            {
                if (Input.GetMouseButtonUp(0))
                {

                    OnMouseClickUp(Vector2.zero);

                }
            }
            if (!FindObjectOfType<VegetableCrush_Controller>())
                Destroy(this.gameObject);
        }

        public void OnMouseDown()
        {
          
        }
        public void OnMouseUp()
        {
           
        }

        void DoAngleMath() 
        {
            if (Mathf.Abs(EndPos.x - StartPos.x) > SwipeResistence || Mathf.Abs(EndPos.y - StartPos.y) > SwipeResistence)
            {
                SwipeAngle = Mathf.Atan2(EndPos.y - StartPos.y, EndPos.x - StartPos.x) * 180 / Mathf.PI;
                MoveVegetable();
                Gameboard.ActualGameState = GameState.Wait;
            } else 
            {
                Gameboard.ActualGameState = GameState.Move;
            }
        }

        void MoveVegetable() 
        {
            if (!IsMatched)
            {
                
                if ((SwipeAngle > -45f && SwipeAngle <= 45f) && Column < Gameboard.Larghezza - 1)
                {
                    OtherVegetable = Gameboard.AllVerdure[Column + 1, Raw];
                    if (!OtherVegetable.GetComponent<VegetableCrush.Vegetable>().IsMatched)
                    {
                        OtherVegetable.GetComponent<VegetableCrush.Vegetable>().Column -= 1;
                        Column += 1;
                    }
                }
                else if ((SwipeAngle > 45f && SwipeAngle <= 135f) && Raw < Gameboard.Altezza - 1)
                {
                    OtherVegetable = Gameboard.AllVerdure[Column, Raw + 1];
                    if (!OtherVegetable.GetComponent<VegetableCrush.Vegetable>().IsMatched)
                    {
                        OtherVegetable.GetComponent<VegetableCrush.Vegetable>().Raw -= 1;
                        Raw += 1;
                    }
                }
                else if ((SwipeAngle > 135f || SwipeAngle <= -135f) && Column > 0)
                {
                    OtherVegetable = Gameboard.AllVerdure[Column - 1, Raw];
                    if (!OtherVegetable.GetComponent<VegetableCrush.Vegetable>().IsMatched)
                    {
                        OtherVegetable.GetComponent<VegetableCrush.Vegetable>().Column += 1;
                        Column -= 1;
                    }
                }
                else if ((SwipeAngle < -45f && SwipeAngle >= -135f) && Raw > 0)
                {
                    OtherVegetable = Gameboard.AllVerdure[Column, Raw - 1];
                    if (!OtherVegetable.GetComponent<VegetableCrush.Vegetable>().IsMatched)
                    {
                        OtherVegetable.GetComponent<VegetableCrush.Vegetable>().Raw += 1;
                        Raw -= 1;
                    }
                }
                
                StartCoroutine(ValidateMove());
            }
        }
        public IEnumerator ValidateMove() 
        {
            yield return new WaitForSeconds(1f);
            if (OtherVegetable != null) 
            { 
                if (!IsMatched && !OtherVegetable.GetComponent<Vegetable>().IsMatched) 
                {
                    OtherVegetable.GetComponent<Vegetable>().Raw = Raw;
                    OtherVegetable.GetComponent<Vegetable>().Column = Column;
                    Raw = PrevRaw;
                    Column = PrevCol;
                    yield return new WaitForSeconds(0.5f);
                    Gameboard.ActualGameState = GameState.Move;
                } else 
                {
                    Gameboard.DestroyAllMatches();
                    
                }
                OtherVegetable = null;
            }
        }
        bool isswipe = false;
        public void OnMouseClickDown(Vector2 mousepos)
        {
            Debug.Log("down " + this.name);
            if (Gameboard.ActualGameState == GameState.Move)
            {
                StartPos = VirtualCursor.instance.CursorPosition;
                isswipe = true;
            }
        }
       
        public void OnMouseClickUp(Vector2 mousepos)
        {
            isswipe = false;
            Debug.Log("up" + this.name);
            if (Gameboard.ActualGameState == GameState.Move)
            {
                EndPos = VirtualCursor.instance.CursorPosition;
                DoAngleMath();
            }
        }

        /*void CheckMatch() 
        {
            if ((Column > 0 && Column < Gameboard.Larghezza - 1) && !IsMatched) 
            {
                GameObject VerduraSx = Gameboard.AllVerdure[Column - 1, Raw];
                GameObject VerduraDx = Gameboard.AllVerdure[Column + 1, Raw];
                if (VerduraSx != null && VerduraDx != null)
                {
                    if (VerduraSx.tag == this.gameObject.tag && VerduraDx.tag == this.gameObject.tag)
                    {
                        VerduraSx.GetComponent<Vegetable>().IsMatched = true;
                        VerduraDx.GetComponent<Vegetable>().IsMatched = true;
                        IsMatched = true;
                    }
                }
            }
             if ((Raw > 0 && Raw < Gameboard.Altezza - 1)&&!IsMatched)
            {
                GameObject VerduraUp = Gameboard.AllVerdure[Column, Raw+1];
                GameObject VerduraDown = Gameboard.AllVerdure[Column, Raw-1];
                if (VerduraUp != null && VerduraDown != null)
                {
                    if (VerduraUp.tag == this.gameObject.tag && VerduraDown.tag == this.gameObject.tag)
                    {
                        VerduraUp.GetComponent<Vegetable>().IsMatched = true;
                        VerduraDown.GetComponent<Vegetable>().IsMatched = true;
                        IsMatched = true;
                    }
                }
            }
            
        }*/
    }
}