﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace VegetableCrush
{
    public class VirtualScreen : GraphicRaycaster
    {
        public Camera screenCamera;

        public GameObject lastSelected;
        public Ray ray;
        // Called by Unity when a Raycaster should raycast because it extends BaseRaycaster.

            /*
        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            
            
            Debug.Log("click");
            Ray ray = eventCamera.ScreenPointToRay(VirtualCursor.instance.CursorPosition); // Mouse
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                RaycastBeyondTV(hit, resultAppendList);
                Debug.Log(hit.transform.name + "colpito");
            }

            
        }
    */
    
        public void RaycastBeyondTV(Vector2 originHit, List<RaycastResult> resultAppendList)
        {
            // Figure out where the pointer would be in the second camera based on texture position or RenderTexture.
            Vector3 virtualPos = new Vector3(originHit.x/ screenCamera.pixelWidth, originHit.y/ screenCamera.pixelHeight);
            ray = screenCamera.ViewportPointToRay(virtualPos);
           // Debug.Log(ray.origin);
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 0.2f);

            RaycastHit2D hit;
            if (hit = Physics2D.Raycast(ray.origin, Vector2.zero))
            {
                lastSelected = hit.collider.gameObject;
                //Debug.Log(hit.collider.gameObject);
                RaycastResult result = new RaycastResult
                {
                    gameObject = hit.collider.gameObject,
                    module = this,
                    distance = hit.distance,
                    index = resultAppendList.Count,
                    worldPosition = hit.point,
                    worldNormal = hit.normal,
                };
                resultAppendList.Add(result);

            }
            else lastSelected = null;
        }
    }
}