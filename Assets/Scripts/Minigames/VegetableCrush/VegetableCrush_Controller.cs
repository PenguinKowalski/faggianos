﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VegetableCrush_Controller : Minigame
{
    public Transform GameBoardRoot,Window;
    public GameObject MainCanvas, VegCrushRoot;
    // Start is called before the first frame update
    public override void SpawnFinestra()
    {
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = VegCrushRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(VegCrushRoot);

        base.SpawnFinestra();
    }
    private void OnDestroy()
    {
        if (GameBoardRoot.transform)
        Destroy(GameBoardRoot.gameObject);
    }

    IEnumerator EnableDelayed()
    {
        yield return new WaitForEndOfFrame();
        GameBoardRoot.parent = null;
        //GameBoardRoot.position = new Vector3(0,20,0);
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = VegCrushRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;



        Destroy(MainCanvas);
        Destroy(VegCrushRoot);
        yield return null;
    }
}
