﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;

public class RotateWindow : Minigame
{
    
    public GameObject gitgud;
    public GameObject tryclick;
    RectTransform parentWindow;

    RectTransform rect;
    public Vector3 dir;
    public float speed = 0f;
    public float rotatespeed = 0f;

    public float height, width;

    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 buttonSize;

    public Vector2 rectPos;
    // Start is called before the first frame update

    void Start()
    {
        GameManager.Instance.ListaFinestreRotanti.Add(this);
        if (GameManager.Instance.ListaFinestreRotanti.Count >1)
        {
            gitgud.SetActive(true);
            tryclick.SetActive(false);
        }
        else 
        {
            gitgud.SetActive(false);
            tryclick.SetActive(true);
        }
        


        parentWindow = this.transform.parent.GetComponent<RectTransform>();

        rect = finestraRef.GetComponent<RectTransform>();
        dir = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;

        height = parentWindow.sizeDelta.y;
        width = parentWindow.sizeDelta.x;

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.ListaFinestreRotanti.Count >= 3)
        {
            for (int i = 0; i < GameManager.Instance.ListaFinestreRotanti.Count; i++)
            {
                
            }
        }
        
        finestraRef.transform.Rotate(new Vector3(0, 0,rotatespeed*Time.deltaTime));
      //  finestraRef.content_half_root.transform.Rotate(new Vector3(0, 0, rotatespeed * Time.deltaTime));
        rect.position += speed * dir;

        rectPos = rect.anchoredPosition;

        borderDown = (parentWindow.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (parentWindow.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (parentWindow.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (parentWindow.position + new Vector3(+width / 2f, 0, 0)).x;

        if (rectPos.y  > borderUp)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            
        }
        if (rectPos.y  < borderDown)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            
        }
        if (rectPos.x  > borderRight)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            
        }
        if (rectPos.x  < borderLeft)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            
        }
    }
    public void YouGud()
    {
        GameManager.Instance.ListaFinestreRotanti.Remove(this);

        Victory();
    }
    public void GitGud()
    {
        Lose();
       
    }
    public override void Lose()
    {
        
        if (GameManager.Instance.ListaFinestreRotanti.Count >=3)
        {
            GameManager.Instance.ClearRotatingWindows();
            GameManager.Instance.Punish(chanceMalus);
            
        }
        else if(GameManager.Instance.ListaFinestreRotanti.Count > 0)
        {
            GameManager.Instance.SendMinigame(24);
        }
    }
    public override void Victory()
    {
        StartCoroutine(CloseDelayed(1));
        if (GameManager.Instance.ListaFinestreRotanti.Count<=0) 
        {
            GameManager.Instance.Reward(chanceBonus);
        }
    }
}