﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour

{
    enum Direction
    {
        Nord, Sud, Est, Ovest
    }
    public float speed;

    Direction movingDir;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if( LeanTween.isTweening(this.gameObject)){
            return;
        }
           

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        if ( x!= 0)
        {
            if (x > 0)
            {
                GoToTarget(Direction.Est);
            }
            else
                GoToTarget(Direction.Ovest);

        }
        else if ( y!= 0)
        {
            if (y > 0)
            {
                GoToTarget(Direction.Nord);
            }
            else
                GoToTarget(Direction.Sud);
           
        }

    }

    void GoToTarget(Direction direzione)
    {
        Vector2 dir = Vector2.zero;
        if (direzione == Direction.Nord) dir = new Vector2(0, 1);
        else if (direzione == Direction.Sud) dir = new Vector2(0, -1);
        else if (direzione == Direction.Est) dir = new Vector2(1, 0);
        else dir = new Vector2(-1, 0);

        Vector3 target;
        RaycastHit2D hit;
        if (hit = Physics2D.Raycast(this.transform.position, dir))
        {
            if (direzione == Direction.Nord) target= hit.collider.transform.position + new Vector3(0,-1,0);

            else if (direzione == Direction.Sud) target = hit.collider.transform.position + new Vector3(0, 1, 0);
            else if (direzione == Direction.Est) target = hit.collider.transform.position + new Vector3(-1, 0, 0);
            else target = hit.collider.transform.position + new Vector3(1, 0, 0);

            int numcaselle =(int) (this.transform.position - target).magnitude;

            LeanTween.moveLocal(this.transform.gameObject, target, speed*numcaselle).setEaseOutQuad();
        }
    }
   


}