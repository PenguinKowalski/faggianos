﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TombManager : MonoBehaviour
{
    public TombController tomb;
    public Transform player;

    public Transform[] monetine;
    public Transform[] nemici;
    public Transform[] zucchine;

    public Transform Exit;

  
    public int punteggio = 0;
    int zucchineRaccolte = 0;

    int valoreZucchina = 30;
    int valoreMoneta = 15;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null) return;

        foreach (Transform t in monetine)
        {
            if (t == null) continue;

            if (CheckDistance(t))
            {
                GameObject.Destroy(t.gameObject);
                punteggio+= valoreMoneta;
            }
        }

        foreach (Transform t in nemici)
        {
            if (t == null) continue;

            if (CheckDistance(t))
            {
                GameObject.Destroy(player.gameObject);
                tomb.Lose();
            }
        }
        foreach (Transform t in zucchine)
        {
            if (t == null) continue;

            if (CheckDistance(t))
            {
                GameObject.Destroy(t.gameObject);
                punteggio += valoreZucchina;
                zucchineRaccolte++;
                
            }
        }

        if (CheckDistance(Exit))
        {
            if (zucchineRaccolte == zucchine.Length)
            {
                tomb.Victory();
                Debug.Log("vittoria");
                GameObject.Destroy(player.gameObject);
            }
        }

    }

    bool CheckDistance(Transform obj)
    {
        if ((obj.position - player.position).sqrMagnitude < 0.5f)
        {
            return true;
        }
        else return false;
    }
}
