﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TombController : Minigame
{
    public Transform GameBoardRoot, Window;
    public GameObject MainCanvas, TombOfTheMask_Root;
    // Start is called before the first frame update
    public override void SpawnFinestra()
    {
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = TombOfTheMask_Root.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(TombOfTheMask_Root);

        base.SpawnFinestra();
    }

    IEnumerator EnableDelayed()
    {
        yield return new WaitForEndOfFrame();
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = TombOfTheMask_Root.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;



        Destroy(MainCanvas);
        Destroy(TombOfTheMask_Root);
        yield return null;
    }
    private void OnDestroy()
    {
        Destroy(GameBoardRoot.gameObject);
    }
}
