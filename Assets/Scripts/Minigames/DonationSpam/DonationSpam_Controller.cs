﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonationSpam_Controller : Minigame
{
    public List<GameObject> PaginaSpam;
    public int donazione;
    private void Start()
    {
        PaginaSpam[Random.Range(0, PaginaSpam.Count)].gameObject.SetActive(true);
    }

    public void WrongDonation()
    {
        GameManager.Instance.ChangeMoney(-donazione);
        Close();
    }

    public void RightDonation()
    {
        GameManager.Instance.ChangeMoney(-donazione);
        GameManager.Instance.Reward(0.8f);
        Close();
    }
}
