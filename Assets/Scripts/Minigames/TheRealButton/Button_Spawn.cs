﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Spawn : MonoBehaviour
{
    public Transform[] buttons;
    int ChangeTime = 8;

    void Start()
    {
        for (int i = 0; i <= ChangeTime; i++)
        {
            int rand1 = Random.Range(0, buttons.Length);
            int rand2 = Random.Range(0, buttons.Length);

            Vector3 temp = buttons[rand1].localPosition;
            buttons[rand1].localPosition = buttons[rand2].localPosition;
            buttons[rand2].localPosition = temp;
        }
    }
}
