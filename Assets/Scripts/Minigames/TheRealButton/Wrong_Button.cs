﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wrong_Button : MonoBehaviour
{
    public GameObject loseScreen;
    public Right_Button Controller;

    void Start()
    {
        Controller = FindObjectOfType<Right_Button>();
        loseScreen.SetActive(false);
    }

    public void OnClick()
    {
        loseScreen.SetActive(true);
        Controller.Lose();
        GameManager.Instance.Punish(Controller.chanceMalus);
        //Controller.Close();
    }
}
