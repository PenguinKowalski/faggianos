﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Right_Button : Minigame
{
    public GameObject winScreen;
   // public TheRealButton_Controller Controller;

    void Start()
    {
        winScreen.SetActive(false);
        //Controller = FindObjectOfType<TheRealButton_Controller>();
    }

    public void OnClick()
    {
        winScreen.SetActive(true);
        Victory();
        GameManager.Instance.Reward(chanceBonus);
        //Controller.Close();
    }
}
