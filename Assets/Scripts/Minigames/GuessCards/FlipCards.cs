﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipCards : MonoBehaviour
{
    public static FlipCards instance;

    public Animator this_animator;

    public void Awake()
    {
        instance = this;
    }
    public void FlipCard_Right()
    {
        if (guessCard_manager.instance.wrong_card == false)
        {
            this_animator.SetTrigger("ChooseCard");
            guessCard_manager.instance.right_card = true;
            guessCard_manager.instance.Victory();
        }
    }

    public void FlipCard_Wrong()
    {
        if (guessCard_manager.instance.right_card == false)
        {
            this_animator.SetTrigger("ChooseCard");
            guessCard_manager.instance.wrong_card = true;
            guessCard_manager.instance.Lose();
        }
    }
}
