﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guessCard_manager : Minigame
{
    public static guessCard_manager instance;

    public GameObject SchermataMain, SchermataCoward;
    public bool wrong_card, right_card;

    public int Total_cards = 10;
    public int correct_cards = 1;

    public List<GameObject> Cards;
    public Transform Grid_Spawner;

    public void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        SpawnCards();
    }

    public void SpawnCards()
    {
        for(int i = 0; i< correct_cards; i++)
        {
            GameObject go = Instantiate(Cards[0], Grid_Spawner);
        }
        for(int i =0; i < Total_cards - correct_cards; i++)
        {
            GameObject go = Instantiate(Cards[1], Grid_Spawner);
        }

        for (int i = 0; i <= correct_cards; i++)
        {
            Grid_Spawner.GetChild(0).SetSiblingIndex(Random.Range(i + 1, Grid_Spawner.childCount));
        }
    }

    public void RejectMinigameOnClick()
    {
        SchermataMain.SetActive(false);
        SchermataCoward.SetActive(true);
        GameManager.Instance.Punish(1f);
        Close();
    }
}
