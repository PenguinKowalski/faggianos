﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Synth_Controller : Minigame
{
    public List <Button> gameButtons;

    public int bleepCount;

    List<int> bleeps;
    List<int> playerBleeps;

    //System.Random rng;

    bool inputEnabled = false;

    void Start()
    {
        StartCoroutine(SynthSays());
    }
    IEnumerator PlayAudio(int index)
    {
        float length = 1.5f;
        float frequency = 0.001f * ((float)index + 1f);

        AnimationCurve volumeCurve = new AnimationCurve(new Keyframe(0f, 1f, 0f, -1f), new Keyframe(length, 0f, -1f, 0f));
        AnimationCurve frequencyCurve = new AnimationCurve(new Keyframe(0f, frequency, 0f, 0f), new Keyframe(length, frequency, 0f, 0f));

        LeanAudioOptions audioOptions = LeanAudio.options();
        audioOptions.setWaveSine();
        audioOptions.setFrequency(44100);

        AudioClip audioClip = LeanAudio.createAudio(volumeCurve, frequencyCurve, audioOptions);

        LeanAudio.play(audioClip, 0.5f);

        gameButtons[index].image.color = Color.grey;
        yield return new WaitForSeconds(0.5f);
        gameButtons[index].image.color = Color.white;
    }
    public void OnGameButtonClick(int index)
    {
        if (!inputEnabled)
        {
            return;
        }

        Bleep(index);

        gameButtons[index].image.color = Color.white;
        playerBleeps.Add(index);


        if (bleeps[playerBleeps.Count - 1] != index)
        { 
            inputEnabled = false;
            Lose();
        }

        if (bleeps.Count == playerBleeps.Count)
        {
            StartCoroutine(SynthSays());
        }
    }

    IEnumerator SynthSays()
    {
        inputEnabled = false;

        SetSynthBleeps();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < bleeps.Count; i++)
        {
            Bleep(bleeps[i]);
            yield return new WaitForSeconds(1f);
        }

        inputEnabled = true;

        yield return null;
    }

    void Bleep(int index)
    {
        StartCoroutine(PlayAudio(index));
    }

    void SetSynthBleeps()
    {
        bleeps = new List<int>();
        playerBleeps = new List<int>();

        for (int i = 0; i < bleepCount; i++)
        {
            int rng = Random.Range(0,gameButtons.Count);
            bleeps.Add(rng);
        }

        bleepCount++;
        if (bleepCount == 6)
        {
            Victory();
        }
          
    }
}

