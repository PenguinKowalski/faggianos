﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiaLingua_Controller : Minigame
{
    public GameObject schermata1, schermata2;

    public CambiaLingua_Controller()
    {
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        finestraRef.scrollClick.AddListener(CambiaPaginaOnClick);
        yield return null;
    }

    public void CambiaPaginaOnClick()
    {
        schermata1.SetActive(false);
        schermata2.SetActive(true);
    }

    public void ButtonOk()
    {
        Victory();

    }

    public void ButtonMaybe()
    {
        StartCoroutine(CloseDelayed(-1));
    }

    public void ButtonPerhaps()
    {
        Lose();
    }

}
