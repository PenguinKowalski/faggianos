﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Akinator_Controller : Minigame
{
    // Start is called before the first frame update
    public GameObject casella1, casella2, casella3, casella4, bottoneGrosso;
    public AudioClip VaBene;
    public AudioSource AudioSauce;

    Animator animator;

    public TextMeshProUGUI domanda;

    public TextMeshProUGUI[] risposte;

    DatasetAkinator dataset;
    int randomcorretta;

    void Start()
    {
        animator = GetComponent<Animator>();
        dataset = GetComponent<DatasetAkinator>();

        animator.enabled = false;
        casella1.SetActive(false);
        casella2.SetActive(false);
        casella3.SetActive(false);
        casella4.SetActive(false);

        SpawnTesti();
    }

    void SpawnTesti()
    {
        int random = Random.Range(0, dataset.database.Count);

        domanda.text = dataset.database[random].domanda;


        randomcorretta = Random.Range(0, 4);

        risposte[randomcorretta].text = dataset.database[random].risposte[0];

        int j = 1; 
        for (int i=0; i< 4; i++)
        {
            if (i == randomcorretta) continue;

            risposte[i].text = dataset.database[random].risposte[j];
            j++;
        }

    }

    public void RispostaOnClick(int indice)
    {
        if (indice == randomcorretta)
        {
            //Debug.Log("risposta giusta");
            AudioSauce.PlayOneShot(VaBene);
            Victory();
        }

        else
        {
            //Debug.Log("risposta sbagliata");
            Lose();
        }
           
    }

    public void NextOnClick()
    {
        if (casella1.activeInHierarchy)
        {
            casella2.SetActive(true);
            bottoneGrosso.SetActive(false);
        }
        else
        {
            casella1.SetActive(true);
        }
          
    }

    public void StartOnClick()
    {
        animator.enabled = true;
        /*
        casella1.SetActive(false);
        casella2.SetActive(false);

        casella3.SetActive(true);
        casella4.SetActive(true);
        */
        animator.Play("transizione");

    }
}
