﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnVirus : MonoBehaviour
{
    public static SpawnVirus instance;

    public List<GameObject> Virus;
    public RectTransform parent_wind;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        /*foreach(GameObject vir in Virus)
            Spawn_Virus();*/
    }

    public void Spawn_Virus()
    {
        GameObject go = Instantiate(Virus[Random.Range(0, Virus.Count)], parent_wind);
        go.transform.SetParent(parent_wind);
    }
}
