﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Button_Utilità_Shop : MonoBehaviour
{
    public int costo;
    public int bonusIndex;

    public TextMeshProUGUI costoTXT;

    public int counter;
    int start_counter;

    public GameObject ChangeIntoThis, Firewall_button;
    public Transform VerticalLayout;
    public int layout_index;


    
    void Start()
    {
        start_counter = counter;
        costoTXT.text = costo.ToString();
    }
    

    public void CheckSpawn()
    {
        if (GameManager.Instance.database.IsBonusBought(bonusIndex)) Spawn();
    }

    public void ShopOnClick()
    {
        if (GameManager.Instance.database.ChangeZucchini(-costo))
        {
            Spawn();
        }
    }


    public void QuarantineShop()
    {
        if (!Firewall_button.gameObject.activeInHierarchy)
        {
            if (GameManager.Instance.database.ChangeZucchini(-costo))
            {
                Spawn();
            }
        }
    }

    void Spawn()
    {
        layout_index = this.transform.GetSiblingIndex();
        this.gameObject.SetActive(false);
        ChangeIntoThis = Instantiate(ChangeIntoThis, VerticalLayout);
        ChangeIntoThis.transform.SetSiblingIndex(layout_index);
        GameManager.Instance.database.BuyBonus(bonusIndex);
    }

    IEnumerator CloseDelayedTimer()
    {
        while (counter > 0)
        {
            string minutes = Mathf.Floor(counter / 60).ToString("00");
            string seconds = (counter % 60).ToString("00");

            counter--;
            yield return new WaitForSeconds(1);
        }
        Time.timeScale = 1;
        counter = start_counter;
        yield return null;
    }

    public void SlowTime()
    {
        if (GameManager.Instance.database.ChangeZucchini(-costo))
        {
            StartCoroutine(CloseDelayedTimer());
            Time.timeScale = 0.5f;
        }
    }

    public void GeneralBlock()
    {
        if (GameManager.Instance.database.ChangeZucchini(-costo))
        {
            GameManager.Instance.RemoveState();
        }
    }

    public void RemoveToolbarAll()
    {
        if (GameManager.Instance.database.ChangeZucchini(-costo))
        {
            GameManager.Instance.CounterToolbar = 0;
        }
    }

    public void SecurityPack()
    {
        if (GameManager.Instance.database.ChangeZucchini(-costo))
        {
            GameManager.Instance.StartBonusRemoveMalus();
        }
    }

}
