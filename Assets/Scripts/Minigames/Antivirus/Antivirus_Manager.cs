﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Antivirus_Manager : Minigame
{
    public static Antivirus_Manager instance;

    public GameObject Antivirus_tab, Quarantine_tab, Utility_Tab;
    public Database db_zucchine;
    public int costo;
    public int percent_escape;

    public Button_Utilità_Shop[] bottoni;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        db_zucchine =GameManager.Instance.database;
    }

    public void CheckSpawn()
    {
        foreach (Button_Utilità_Shop b in bottoni)
        {
            b.CheckSpawn();
        }
    }
    //Funzioni per le schede laterali
    public void OpenQuarantenaOnClick()
    {
        Antivirus_tab.SetActive(false);
        Utility_Tab.SetActive(false);
        Quarantine_tab.SetActive(true);
    }
    public void OpenAntivirusSideOnClick()
    {
        Utility_Tab.SetActive(false);
        Antivirus_tab.SetActive(true);
    }
    public void OpenUtilityOnClick()
    {
        Antivirus_tab.SetActive(false);
        Utility_Tab.SetActive(true);
    }

    public void CostoPocoOnClick()
    {
        if (db_zucchine.ChangeZucchini(-costo))
            db_zucchine.ChangeZucchini(-costo);
    }

    public void CloseAntivirus()
    {
        this.GetComponent<CanvasGroup>().alpha = 0f;
        this.GetComponent<CanvasGroup>().interactable = false;
        this.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public IEnumerator RNG_Escape()
    {
        while (true)
        {
            percent_escape = Random.Range(1, 10);
            yield return new WaitForSeconds(5f);
        }
    }
}
