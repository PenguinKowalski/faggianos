﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button_Antivirus_Shop : MonoBehaviour
{
    public int costo;
    public int bonusIndex;
    public GameObject Clean, CompleteAV;

    Canvas canvas;
    public Database db_zucchine;


    void Start()
    {
        db_zucchine = GameManager.Instance.database;
        canvas = this.gameObject.GetComponentInParent<Canvas>();
    }

    public void CheckSpawn()
    {
        if (GameManager.Instance.database.IsBonusBought(bonusIndex)) Spawn();
    }

    public void ShopThisOnClick()
    {
        if (db_zucchine.ChangeZucchini(-costo))
        {
            GameObject go = Instantiate(Clean, canvas.transform);
        }
    }

    public void Spawn()
    {
        Button btn = this.GetComponent<Button>();
        btn.enabled = false;
        btn.image.color = Color.grey;
        GameManager.Instance.database.BuyBonus(bonusIndex);
    }

    public void AvCompleteOnClick()
    {
        if (db_zucchine.ChangeZucchini(-costo))
        {
            CompleteAV.SetActive(false);
        }
    }
}
