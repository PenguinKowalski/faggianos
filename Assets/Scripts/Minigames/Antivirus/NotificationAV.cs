﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotificationAV : MonoBehaviour
{
    public TextMeshProUGUI[] notifiche_txt;
    public GameObject baloon;

    private void Start()
    {
        StartCoroutine(SpamNotifiche());
    }

    IEnumerator SpamNotifiche()
    {
        while (true)
        {
            int i = Random.Range(0, notifiche_txt.Length);

            this.GetComponent<CanvasGroup>().alpha = 1f;
            this.GetComponent<CanvasGroup>().interactable = false;
            this.GetComponent<CanvasGroup>().blocksRaycasts = false;
            notifiche_txt[i].gameObject.SetActive(true);
            yield return new WaitForSecondsRealtime(5f);
            notifiche_txt[i].gameObject.SetActive(false);
            this.GetComponent<CanvasGroup>().alpha = 0f;
            this.GetComponent<CanvasGroup>().interactable = false;
            this.GetComponent<CanvasGroup>().blocksRaycasts = false;
            yield return new WaitForSecondsRealtime(3f);
        }
    }
}
