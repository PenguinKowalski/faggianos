﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quarantine_Manager : MonoBehaviour
{
    public RectTransform parentWindow;

    public int escape, lifetime;

    public Image immagine_virus;

    public Canvas canvas;

    RectTransform rect;
    public Vector3 dir;
    public float speed, start_speed;


    private Vector3 initial_pos;

    public float height, width;

    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 buttonSize;

    public Vector2 rectPos;

    void Start()
    {
        start_speed = speed;
        initial_pos = this.transform.position;

        parentWindow = this.transform.parent.GetComponent<RectTransform>();
        canvas = this.gameObject.GetComponentInParent<Canvas>();

        rect = this.GetComponent<RectTransform>();
        dir = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;

        height = parentWindow.sizeDelta.y;
        width = parentWindow.sizeDelta.x;

        this.transform.SetParent(parentWindow);

        buttonSize = rect.sizeDelta * 0.5f;
    }

    private void Update()
    {
        VirusDeath();

        if (Antivirus_Manager.instance.Utility_Tab.gameObject.activeInHierarchy ||
            Antivirus_Manager.instance.Antivirus_tab.gameObject.activeInHierarchy)
        {
            immagine_virus = this.gameObject.GetComponent<Image>();
        }

        rect.position += speed * dir;

        rectPos = rect.anchoredPosition;

        if (escape == 1)
        {
          this.transform.SetParent(canvas.transform);
          height = canvas.GetComponent<RectTransform>().sizeDelta.y;
          width = canvas.GetComponent<RectTransform>().sizeDelta.x;

          borderDown = (canvas.transform.position + new Vector3(0, -height / 2f, 0)).y;
          borderUp = (canvas.transform.position + new Vector3(0, height / 2f, 0)).y;
          borderLeft = (canvas.transform.position + new Vector3(-width / 2f, 0, 0)).x;
          borderRight = (canvas.transform.position + new Vector3(+width / 2f, 0, 0)).x;
        }

        if (this.transform.IsChildOf(parentWindow))
        {
            height = parentWindow.sizeDelta.y;
            width = parentWindow.sizeDelta.x;

            borderDown = (parentWindow.position + new Vector3(0, -height / 2f, 0)).y;
            borderUp = (parentWindow.position + new Vector3(0, height / 2f, 0)).y;
            borderLeft = (parentWindow.position + new Vector3(-width / 2f, 0, 0)).x;
            borderRight = (parentWindow.position + new Vector3(+width / 2f, 0, 0)).x;
        }

        Vector3 thescale = this.transform.localScale;
        if (rectPos.y + buttonSize.y > borderUp)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            escape = Random.Range(1, 10);
            if (this.transform.IsChildOf(parentWindow))
            {
                escape = Random.Range(1, 10);
                lifetime--;
            }
        }
        if (rectPos.y - buttonSize.y < borderDown)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            escape = Random.Range(1, 10);
            if (this.transform.IsChildOf(parentWindow))
            {
                escape = Random.Range(1, 10);
                lifetime--;
            }
        }
        if (rectPos.x + buttonSize.x > borderRight)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            thescale.x *= -1;
            this.transform.localScale = thescale;
            escape = Random.Range(1, 10);
            if (this.transform.IsChildOf(parentWindow))
            {
                escape = Random.Range(1, 10);
                lifetime--;
            }
        }
        if (rectPos.x - buttonSize.x < borderLeft)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            thescale.x *= -1;
            this.transform.localScale = thescale;
            if (this.transform.IsChildOf(parentWindow))
            {
                escape = Random.Range(1, 10);
                lifetime--;
            }
                
        }
    }
    public void OnBeginDrag()
    {
        escape = 0;
        this.transform.SetParent(VirtualCursor.instance.transform);
        speed = 0f;
    }

    public void onMidDrag()
    {
        escape = 0;
        this.transform.SetParent(VirtualCursor.instance.transform);
    }

    public void OnEndDrag()
    {
        escape = 0;
        if (Mathf.Abs(transform.position.x - parentWindow.transform.position.x) <= 100f &&
            Mathf.Abs(transform.position.y - parentWindow.transform.position.y) <= 100f)
        {
            this.transform.SetParent(parentWindow);
            speed = start_speed;
        }
        Antivirus_Manager.instance.percent_escape = Random.Range(1, 10);
    }

    public void VirusDeath()
    {
        if (lifetime == 0)
            Destroy(this.gameObject);
    }
}
