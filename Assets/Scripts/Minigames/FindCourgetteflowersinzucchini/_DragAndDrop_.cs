﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _DragAndDrop_ : MonoBehaviour
{
    bool isDragging = false;

    Transform cursore;

    void Start()
    {
        cursore = VirtualCursor.instance.gameObject.transform;
    }

    void Update()
    {
        if (isDragging)
        {
            this.transform.position = cursore.position;
        }
    }

    public void OnPointerDown()
    {
        isDragging = true;
    }

    public void OnPointerUp()
    {
        isDragging = false;
    }
}
