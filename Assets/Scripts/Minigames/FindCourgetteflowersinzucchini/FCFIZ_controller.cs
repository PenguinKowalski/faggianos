﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCFIZ_controller : Minigame
{
   public int zucchinispawn = 50;
    public int courgetteflower = 10;
    public int zucchiniRange, courgetteRange;

    public float minWindowX=-300f, maxWindowX=300f, minWindowY=-250f, maxWindowY=250f;

    public GameObject zucchiniPrefab, courgettePrefab;

    public Transform utility;

    public int CourgetteSpawnati, TestoInserito;

    void Start()
    {      
        SpawnCourgetteflower();
        SpawnZucchini();
    }

   
    void SpawnCourgetteflower()
    {
        int randomCourgetteflower = Random.Range(courgetteflower - courgetteRange, courgetteflower + courgetteRange);
        CourgetteSpawnati = randomCourgetteflower;
        for (int i = 0; i < randomCourgetteflower; i++)
        {
            GameObject go = GameObject.Instantiate(courgettePrefab, utility);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(Random.Range(minWindowX, maxWindowX), Random.Range(minWindowY, maxWindowY));
            rt.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));

        }
    }


    void SpawnZucchini()
    {
        int randomZucchini = Random.Range(zucchinispawn - zucchiniRange, zucchinispawn + zucchiniRange);
        for (int i = 0; i < randomZucchini; i++)
        {
            GameObject go = GameObject.Instantiate(zucchiniPrefab, utility);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(Random.Range(minWindowX, maxWindowX), Random.Range(minWindowY, maxWindowY));
            rt.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));

        }
    }

    public void VerifyButtonOnClick()
    {
        if (CourgetteSpawnati == TestoInserito) Victory();
        else Lose();

    }

    public void UpdateNumberValueOnChangeValue(string newvalue)
    {
        TestoInserito = int.Parse(newvalue);

    }
}
