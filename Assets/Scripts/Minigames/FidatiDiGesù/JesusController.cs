﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JesusController : Minigame
{
    public TextMeshProUGUI noFaithText;
    public GameObject YesButton, NoButton, Jesoo, trustText;

    public void YesOnClick()
    {
        //GameManager.Instance.RemoveHalfState();       Da scommentare
        Victory();
    }

    public override void Victory()
    {
        StartCoroutine(CloseDelayed(1));
        if (SchermataVictory)
            SchermataVictory.SetActive(true);
        GameManager.Instance.GiveState("JollyGesù");
    }

    public void NoOnClick()
    {
        noFaithText.gameObject.SetActive(true);

        YesButton.gameObject.SetActive(false);
        NoButton.gameObject.SetActive(false);
        Jesoo.gameObject.SetActive(false);
        trustText.gameObject.SetActive(false);

        GameManager.Instance.Punish(1f);
        Lose();
    }
}
