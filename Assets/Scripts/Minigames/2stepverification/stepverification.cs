﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class stepverification : Minigame
{
    const string caratteri = "0123456789";

    public GameObject enable;
    public bool isActive;
    public bool isCorrect;

    public TMP_InputField zonatesto;

    public string codice;

    int charAmount = 10;
    void Start()
    {
        for (int i = 0; i < charAmount; i++)
            codice += caratteri[Random.Range(0, caratteri.Length)];

        

        GameManager.Instance.MailDynamicVariables["cod2Step"] = codice;
        GameManager.Instance.SendMail("23738"); //codice mail da fare

    }



    public void ButtonVerifyOnClick()
    {
        isActive = !isActive;

        if (zonatesto.text.Equals(codice))
        {
            Victory();
        }
        else
        {
            Lose();
        }

    }
    public void ButtonCancelOnClick()
    {
        zonatesto.text = "";
    }
    /*public void TextOnChange(string text)
    {

    }
    */
}
