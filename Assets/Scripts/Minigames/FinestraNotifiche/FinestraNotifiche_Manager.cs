﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinestraNotifiche_Manager : Minigame
{
    
    public string malusCod="notificationspammer";
    public GameObject YesResp, NoResp;
    public FinestraNotifiche_Manager()
    {
    }

    public void OnCrossClick() { Victory(); }
    public void OnYesButtonClick() 
    {
        YesResp.SetActive(true);
        Lose();
      
    }
    public void OnNoButtonClick()
    {
        NoResp.SetActive(true);
        Lose();

    }
    public override void Lose() 
    {
        StartCoroutine(CloseDelayed(0));
        //SchermataLose.SetActive(true);
        GameManager.Instance.Punish(chanceMalus, malusCod);
    }
}
