﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.ComponentModel;

public class popeCounter_controller : MonoBehaviour, iMinigameEvent
{
    public GameObject Pope_Object;
    public TextMeshProUGUI CounterText;
    public int counter, minigamesTowin;


    private void OnEnable()
    {
        GameManager.Instance.AddListenerMinigameEvent(this);
    }
    public void OnDestroy()
    {
        GameManager.Instance.RemoveListenerMinigameEvent(this);
    }
    void Update()
    {
        CounterText.text = "Minigiochi completati: " + counter + "/" + minigamesTowin;
    }

    public void CloseOnClick()
    {
        Pope_Object.GetComponent<CanvasGroup>().alpha = 0f;
        Pope_Object.GetComponent<CanvasGroup>().interactable = false;
        Pope_Object.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void CounterWin()
    {
        counter++;
        if (counter == 5)
        {
            GameManager.Instance.Reward(1f);
            Destroy(this.gameObject);
        }
    }

    public void CounterLose()
    {
        GameManager.Instance.Punish(1f);
        Destroy(this.gameObject);
    }

    public void OnGameWin()
    {
        CounterWin();
    }

    public void OnGameLose()
    {
        CounterLose();
    }

    public void OnGameStart(int index)
    {
        return;
    }
}
