﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GuadagnaCasa : Minigame
{
    public int A;
    public GameObject Hardest, Packet, Window;


    private void Start()
    {
        A = Random.Range(1, 2);
    }

    // Start is called before the first frame update

    public void OnClick()
    {
        Window.SetActive(false);

       

        if (A ==1)
        {
            Packet.SetActive(true);
            
        }

        else if (A==2)
        {
            
            Hardest.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
