﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : Minigame
{
    public GameObject Packet, Bucket;

 
    // Start is called before the first frame update
    void Start()
    {
        Packet.SetActive(false);
        Bucket.SetActive(true);
        
    }

    public void OnClick()
    {
        Bucket.SetActive(false);
        Packet.SetActive(true);
    }
    // Update is called once per frame
   
}
