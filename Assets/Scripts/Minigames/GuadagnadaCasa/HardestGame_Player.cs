﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardestGame_Player : Minigame
{
    public float speed;
    public Rigidbody2D RB;
    void Start()
    {
        RB = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0)
            RB.velocity = new Vector2(x * speed * 10 * Time.deltaTime, 0);

        if (y != 0)
            RB.velocity = new Vector2(0, y * speed * 10 * Time.deltaTime);

    }
}