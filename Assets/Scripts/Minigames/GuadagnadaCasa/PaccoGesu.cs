﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaccoGesu : MonoBehaviour
{
    public GameObject Gesu, Find;

    // Start is called before the first frame update
    void Start()
    {
        Gesu.SetActive(false);
        
    }

    public void OnClick()
    {
        Gesu.SetActive(true);
        Find.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
