﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sicurofrat_controller : Minigame
{
    public GameObject schermata1, schermata2, schermata3, testopiusicuronellavita;

    private void Start()
    {
        schermata2.SetActive(false);
        schermata3.SetActive(false);
        schermata1.SetActive(true);
    }
    public void FirstOnestoOnClick()
    {
        //chiusura
        Victory();
    }

    public void FirstNonsonointeressatoOnClick()
    {
        schermata1.SetActive(false);
        schermata2.SetActive(true);
        testopiusicuronellavita.SetActive(false);
    }

    public void FirstZucchinegratisOnClick()
    {
        //lose
        Lose();
    }

    public void SecondPuodarsiOnClick()
    {
        testopiusicuronellavita.SetActive(true); //lose

    }

    public void SecondNoOnClick()
    {
        schermata2.SetActive(false);
        schermata1.SetActive(true);
    }

    public void SecondSiOnClick()
    {
        schermata2.SetActive(false);
        schermata3.SetActive(true);
    }

    public void ThirdOkOnClick()
    {
        schermata3.SetActive(false);
        schermata1.SetActive(true);
    }

    public void ThirdNoOnClick()
    {
        //critical lose
        Lose();
    }
}
