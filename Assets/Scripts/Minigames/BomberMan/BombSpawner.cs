﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace BomberMan
{
    public class BombSpawner : MonoBehaviour
    {
        public Tilemap Playground;
        public GameObject Player, BombPref;
        void Update()
        {
            if (Input.GetButtonDown("Jump")) 
            {
                //Vector3 WorldPos = Camera.main.ScreenToWorldPoint(Player.transform.position);
                Vector3Int cell = Playground.WorldToCell(Player.transform.position);
                Vector3 CellCenterPos=Playground.GetCellCenterWorld(cell);

                Instantiate(BombPref, CellCenterPos, Quaternion.identity);
            }
        }
    }
}