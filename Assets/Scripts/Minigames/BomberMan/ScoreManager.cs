﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace BomberMan { 
    public class ScoreManager : MonoBehaviour
    {
        public TextMeshProUGUI TimeText, ScoreText; 
        public GameObject WinText, LoseText;
        public float Score=0, Timer=30f;
        public int EnemyCount;
        public BomberMan_Controller BMController;
        public BomberManMove PlayerMover;
        


        


        void Update()
        {
           
            Timer -= Time.deltaTime;
            TimeText.text = Timer.ToString("#.00");
            ScoreText.text = Score.ToString();
            if (Timer <= 0) 
            {
                // GameOver();
                LoseText.SetActive(true);
                BMController.Lose();
                
            }
            if (EnemyCount <= 0 && !LoseText.activeSelf) 
            {
                //Win();
                PlayerMover.WinAnim();
                WinText.SetActive(true);
                BMController.Victory();
            }
        }

        public void GameOver() 
        {
            //Debug.Log("GO");
            //LoseText.SetActive(true);
            BMController.Lose();
            //Time.timeScale = 0;
        }
        void Win() 
        {
            //Debug.Log("Win");
            //WinText.SetActive(true);
            BMController.Victory();
           // Time.timeScale = 0;
        }
        public void AddScore(int points) 
        {
            Score += points;
        }
        public void RemoveEnemy(int casualties) 
        {
            EnemyCount -= casualties;
        }
    }
}
