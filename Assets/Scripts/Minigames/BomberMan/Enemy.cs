﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BomberMan
{
    public class Enemy : MonoBehaviour
    {
        private Rigidbody2D EnemyRb;
        public float speed;
        private float t, x, y;
        private ScoreManager ScoreManager;
        private void Awake()
        {
            EnemyRb = this.GetComponent<Rigidbody2D>();
            ScoreManager = FindObjectOfType<BomberMan.ScoreManager>();
        }
        private void Start()
        {
            //ChangeDir();
            t = 2.5f;
        }
        void Update()
        {
            t -= Time.deltaTime;
            if (t <= 0) 
            {
                ChangeDir();
                t = 2.5f;
            }

            if (x != 0)
                EnemyRb.velocity = new Vector2(x * speed * 10 * Time.deltaTime, 0);

            if (y != 0)
                EnemyRb.velocity = new Vector2(0, y * speed * 10 * Time.deltaTime);

            if (!FindObjectOfType<BomberMan_Controller>())
                Destroy(this.gameObject);

        }
        private void ChangeDir() 
        {
            int r = Random.Range(0, 3);
            switch(r)
            {
                case 0:
                    x = 1;
                    y = 0;
                    break;
                case 1:
                    x = -1;
                    y = 0;
                    break;
                case 2:
                    x = 0;
                    y = 1;
                    break;                    
                default:
                    x = 0;
                    y = -1;
                    break;
            };
            
            //x = Random.Range(-1,1);
            //y = Random.Range(-1, 1);
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            //Debug.Log("Collision");
            ChangeDir();
        }
        private void OnDestroy()
        {
            if (ScoreManager)
            {
                ScoreManager.AddScore(200);
                ScoreManager.RemoveEnemy(1);
            }
          
        }
    }
}
