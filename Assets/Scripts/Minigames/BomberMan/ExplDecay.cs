﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BomberMan
{
    public class ExplDecay : MonoBehaviour
    {
        public float Countdown = 1f;
        private BomberMan.ScoreManager ScoreMan;
        private void Awake()
        {
            ScoreMan = FindObjectOfType<BomberMan.ScoreManager>();
        }
        void Update()
        {
            Countdown -= Time.deltaTime;
            if (Countdown <= 0)
            {
                Destroy(gameObject);
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject Other = collision.gameObject;
            if (Other.tag =="Player")
            {
                Other.GetComponent<BomberManMove>().Death();
            }
            else 
            {             
                if (Other.tag=="Zucchina")
                    ScoreMan.RemoveEnemy(1);
                Destroy(Other);
            }
        }
    }
}
