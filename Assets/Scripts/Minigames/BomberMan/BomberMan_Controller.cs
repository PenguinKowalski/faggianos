﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberMan_Controller : Minigame 
{
    public Transform GameBoardRoot, Window;
    public GameObject MainCanvas, BomberManRoot;

    public BomberMan_Controller()
    {
    }

    // Start is called before the first frame update
    public override void SpawnFinestra()
    {
        base.SpawnFinestra();
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = BomberManRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(BomberManRoot);
    }

    IEnumerator EnableDelayed()
    {
        yield return new WaitForEndOfFrame();
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = BomberManRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;



        Destroy(MainCanvas);
        Destroy(BomberManRoot);
        yield return null;
    }

    private void OnDestroy()
    {
        Destroy(BomberManRoot);
    }
}
