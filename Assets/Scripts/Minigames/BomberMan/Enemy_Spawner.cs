﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BomberMan
{
    public class Enemy_Spawner : MonoBehaviour
    {
        private int EnemyNumber;
        public Transform[] PossibleSpawns;
        public GameObject EnemyPref;
        private ScoreManager ScoreManager;
        private void Awake()
        {
            ScoreManager = FindObjectOfType<BomberMan.ScoreManager>();
            EnemyNumber = ScoreManager.EnemyCount;

        }

        void Start()
        {
            for (int i=0; i <=EnemyNumber; i++) 
            {
                int k = Random.Range(0,PossibleSpawns.Length);
                Instantiate(EnemyPref, PossibleSpawns[k].position, Quaternion.identity);
            }
        }


    }
}
