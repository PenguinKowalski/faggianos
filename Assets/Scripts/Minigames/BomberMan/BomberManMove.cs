﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BomberMan 
{ 
    public class BomberManMove : MonoBehaviour
    {
        private Rigidbody2D PlayerRb;
        public float speed;
        private ScoreManager ScoreManager;
        public Animator anim;
        private void Awake()
        {
            PlayerRb = this.GetComponent<Rigidbody2D>();
            ScoreManager = FindObjectOfType<BomberMan.ScoreManager>();
            anim = this.GetComponent<Animator>();
        }
        void Update()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            if(x!=0)
            PlayerRb.velocity = new Vector2(x*speed*5 * Time.deltaTime,0);
            Debug.Log(x);

            anim.SetFloat("X",x);
            anim.SetFloat("Y", y);
     
            if (y != 0)
                PlayerRb.velocity = new Vector2( 0,y * speed * 5* Time.deltaTime);
           
        }
        public void Death() 
        {
            StartCoroutine(DeathCO());
        }
        public void WinAnim() 
        {
            anim.SetTrigger("Win");
        }

        IEnumerator DeathCO() 
        {
            anim.SetTrigger("Lose");
            yield return new WaitForSeconds(1f);
            ScoreManager.GameOver();
            Destroy(this.gameObject);
        }
        /*private void OnDestroy()
        {
            anim.SetTrigger("Lose");
            ScoreManager.GameOver();
            
        }*/
    }
}
