﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace BomberMan 
{    
    public class Object_Destruction : MonoBehaviour
    {
        public Tilemap Playground;
        public Tile Wall, Destructible;
        public int WallSpawnProb=10;
        public GameObject ExplosionPref;
        private ScoreManager ScoreManager;
        private void Awake()
        {
            ScoreManager = FindObjectOfType<BomberMan.ScoreManager>();
        }
        public void ExplPos(Vector2 worldPos) 
        {
            Vector3Int OriginPos=Playground.WorldToCell(worldPos);
            Expuroshion(OriginPos);           
            if(Expuroshion(OriginPos+ new Vector3Int(1,0,0)))
            Expuroshion(OriginPos + new Vector3Int(2, 0, 0));
            
            if(Expuroshion(OriginPos + new Vector3Int(-1, 0, 0)))
            Expuroshion(OriginPos + new Vector3Int(-2, 0, 0));

            if(Expuroshion(OriginPos + new Vector3Int(0, 1, 0)))
            Expuroshion(OriginPos + new Vector3Int(0, 2, 0)); 

            if (Expuroshion(OriginPos + new Vector3Int(0, -1, 0)))
            Expuroshion(OriginPos + new Vector3Int(0, -2, 0));
        }
        bool Expuroshion(Vector3Int Cell) 
        {
            Tile TargetTile = Playground.GetTile<Tile>(Cell);
            if (TargetTile == Wall)
            {

                return false;
            }
            else if (TargetTile == Destructible)
            {
                int AssHole = Random.Range(0,100);
                if (AssHole <= WallSpawnProb)
                {
                    Playground.SetTile(Cell, Wall);
                }
                else
                {
                    Playground.SetTile(Cell, null);
                    ScoreManager.AddScore(50);
                }

            }
           
                Vector3 pos = Playground.GetCellCenterWorld(Cell);
                Instantiate(ExplosionPref, pos, Quaternion.identity);

            return true;
        }
    }
}
