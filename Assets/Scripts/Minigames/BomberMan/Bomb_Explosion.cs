﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BomberMan
{
    public class Bomb_Explosion : MonoBehaviour
    {
        public float Countdown = 2f;        
        void Update()
        {
            Countdown -= Time.deltaTime;
            if (Countdown <= 0) 
            {
                //Debug.Log("EXPUROSHION!");
                FindObjectOfType<Object_Destruction>().ExplPos(transform.position);
                Destroy(gameObject);
            }
        }
    }
}
