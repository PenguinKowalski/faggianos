﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
//suca mondo

public class CaptchaImmagini : Minigame
{
    public Sprite[] listaTextureCorrette;
    public Sprite[] listaTextureIncorrette;

    public Sprite[] logoSprites;
    public Image logo;

    public GameObject prefabButton;
    public Transform layoutGrid;

    public TextMeshProUGUI testoZucchina;
    public List<CaptchaButton> bottoni;

    public GameObject infoButton;
    public GameObject textPrefab;

    private AudioSource audioFagiano;

    public void Start()
    {
        SpawnButtons();


        testoZucchina.text = "Faggiano";
        logo.sprite = logoSprites[0];

        audioFagiano = GetComponent<AudioSource>();
    }

    public void OnEnter()
    {
        testoZucchina.text = "Zucchina";
        logo.sprite = logoSprites[1];
    }
    public void OnExit()
    {
        testoZucchina.text = "Faggiano";
        logo.sprite = logoSprites[0];
    }
    public void SpawnButtons()
    {
        int corretti = Random.Range(1, 4);
        bottoni = new List<CaptchaButton>();
        for (int i = 0; i < corretti; i++)
        {
            GameObject go = GameObject.Instantiate(prefabButton, layoutGrid);
            CaptchaButton cp = go.GetComponent<CaptchaButton>();
            bottoni.Add(cp);
            cp.Init(listaTextureCorrette[Random.Range(0, listaTextureCorrette.Length)], true);
        }
        for (int i = 0; i < 9 - corretti; i++)
        {
            GameObject go = GameObject.Instantiate(prefabButton, layoutGrid);
            CaptchaButton cp = go.GetComponent<CaptchaButton>();
            bottoni.Add(cp);
            cp.GetComponent<CaptchaButton>().Init(listaTextureIncorrette[Random.Range(0, listaTextureIncorrette.Length)], false);
        }

        for (int i = 0; i <= corretti; i++)
        {
            layoutGrid.GetChild(0).SetSiblingIndex(Random.Range(i + 1, layoutGrid.childCount));
        }
    }
    public void VerifyOnClick()
    {
        bool isWinning = true;

        for (int i = 0; i < bottoni.Count; i++)
        {
            if (bottoni[i].isActive && !bottoni[i].isCorrect)
            {
                isWinning = false;
            }
            else if (!bottoni[i].isActive && bottoni[i].isCorrect)
            {
                isWinning = false;
            }
        }

        if (isWinning)
        {
            Debug.Log("ho vinto");
            Victory();
        }
        else
        {
            Debug.Log("ho perso");
            Lose();
        }
    }

    public void infoPress()
    {
        textPrefab.SetActive(true);
        StartCoroutine(ActivationRoutine());
    }

    private IEnumerator ActivationRoutine()
    {
        //Wait for 1.5 secs.
        yield return new WaitForSeconds(1.5f);
        textPrefab.SetActive(false);
    }

    public void audioPress()
    {
        audioFagiano.Play();
    }
}
