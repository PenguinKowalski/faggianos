﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptchaButton : MonoBehaviour
{
    private Animator anim;
    public GameObject enable;
    // Start is called before the first frame update
    public bool isActive;
    // Start is called before the first frame update
    Sprite texture;

    public Image immagine;
    public bool isCorrect;
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    public void Init(Sprite _texture, bool corretto)
    {
        enable.SetActive(false);
        texture = _texture;
        isCorrect = corretto;
        immagine.sprite = texture;
    }
    public void OnButtonClick()
    {
        isActive = !isActive;
        if (isActive)
        {
            anim.Play("Scale");
            enable.SetActive(true);
        }
        else
        {
            anim.Play("Unscale");
            enable.SetActive(false);
        }
    }
}
