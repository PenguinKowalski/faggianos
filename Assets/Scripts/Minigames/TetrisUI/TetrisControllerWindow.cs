﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TetrisControllerWindow : Minigame
{
    public Transform GameBoardRoot, Window;
    public GameObject MainCanvas, TetrisRoot;
    // Start is called before the first frame update
    public override void SpawnFinestra()
    {
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = TetrisRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;
        Destroy(MainCanvas);
        Destroy(TetrisRoot);

        base.SpawnFinestra();
    }

    IEnumerator EnableDelayed()
    {
        yield return new WaitForEndOfFrame();
        GameBoardRoot.parent = null;
        GameBoardRoot.position = Vector3.zero;
        GameBoardRoot.localScale = Vector3.one;
        Window.parent = TetrisRoot.transform.parent;
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;



        Destroy(MainCanvas);
        Destroy(TetrisRoot);
        yield return null;
    }
    private void OnDestroy()
    {
        Destroy(GameBoardRoot.gameObject);
    }
}
