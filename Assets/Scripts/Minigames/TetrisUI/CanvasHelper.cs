﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using TMPro;

namespace Faggianos.Tetris
{
	/// <summary>
	/// Deals with menu, buttons and small UI related tasks
	/// </summary>
	public class CanvasHelper : MonoBehaviour
	{

		[Header("UI references")]
		[SerializeField] GameObject GameOverPanel;
		[SerializeField] GameObject GameOverWinPanel;

		[SerializeField] TextMeshProUGUI ScoreText;
		[SerializeField] TextMeshProUGUI GameOverScoreText;


		private int _latestScore = 0;
		private GameManager _manager;

		void Start()
		{
			_manager = GameObject.FindObjectOfType<GameManager>();
		}



		public void ShowGame()
		{
			GameOverPanel.SetActive(false);
			GameOverScoreText.text = _latestScore.ToString();
		}

		public void ShowGameOver()
		{
			GameOverPanel.SetActive(true);
			GameOverScoreText.text = _latestScore.ToString();
		}
		public void ShowGameOverWin()
		{
			GameOverWinPanel.SetActive(true);
			GameOverScoreText.text = _latestScore.ToString();
		}

		public void Reload()
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}

		public void ResetGameArea()
		{
			SetScore(0);
		}


		public int ReadLevel()
		{
			int level = 5;
			PlayerPrefs.SetInt("Level", level);
			return level;
		}

		public void SetScore(int score)
		{
			_latestScore = score;
			ScoreText.text = score.ToString();
		}
	}
}