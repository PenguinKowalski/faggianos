﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvertiMouse : Minigame
{
    public GameObject inverti;
    public GameObject raddrizza;

    bool isinverse;
    private void Start()
    {
        inverti.SetActive(true);
        raddrizza.SetActive(false);
    }
    public void InvertedOn()
    {
        
        VirtualCursor.instance.isInverted = true;
        inverti.SetActive(false);
        raddrizza.SetActive(true);
        isinverse = true;

    }
    public void InvertedOff()
    {
        VirtualCursor.instance.isInverted = false;
        Victory();
    }

    public override void Lose()
    {
        if (isinverse)
            VirtualCursor.instance.isInverted = false;
        base.Lose();
    }
}
