﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveButton : MonoBehaviour
{
    RectTransform parentWindow;

    RectTransform rect;
    public Vector3 dir;
    public float speed = 1f;

    public float height, width;

    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 buttonSize;

    public Vector2 rectPos;
    // Start is called before the first frame update
    void Start()
    {
        parentWindow = this.transform.parent.GetComponent<RectTransform>();

        rect = this.GetComponent<RectTransform>();
        dir = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;

        height = parentWindow.sizeDelta.y;
        width = parentWindow.sizeDelta.x;


        buttonSize = rect.sizeDelta*0.5f;

    }

    // Update is called once per frame
    void Update()
    {
        rect.position += speed * dir;

        rectPos = rect.anchoredPosition;

        borderDown = (parentWindow.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (parentWindow.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (parentWindow.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (parentWindow.position + new Vector3(+width / 2f, 0, 0)).x;

        if (rectPos.y + buttonSize.y > borderUp)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
        }
        if (rectPos.y - buttonSize.y < borderDown)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
        }
        if (rectPos.x + buttonSize.x > borderRight)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
        }
        if (rectPos.x - buttonSize.x < borderLeft)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
        }
    }
}