﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Dinousauro_Controller : Minigame
{
    // Start is called before the first frame update

    public GameObject[] cactusprefabs;
    public Transform parentcactus, start,end;
    public float timeTodespawn;

    public int Score = 0;
    public int ScoreVictory = 10;
    public TextMeshProUGUI testoScore;
    public float timeRespawnMin, timeRespawnMax;

    public Transform player;

    public bool canJump = true;
    public bool isLose = false;
    void Start()
    {
        StartCoroutine(SpawnCactus());
        StartCoroutine(Losing());
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }


        for (int i=0; i< parentcactus.childCount; i++)
        {
            float distance = Vector3.Distance(parentcactus.GetChild(i).transform.position, player.position);
            if (distance < 0.65f)
            {
                _Lose();
            }
        }
    }

    IEnumerator SpawnCactus()
    {
        while (true)
        {
            GameObject cactus = GameObject.Instantiate(cactusprefabs[Random.Range(0, cactusprefabs.Length)],parentcactus);
            cactus.transform.position = start.position ;
            StartCoroutine(CactusCycle(cactus));
            yield return new WaitForSeconds(Random.Range(timeRespawnMin,timeRespawnMax));
        }
    }
    IEnumerator CactusCycle(GameObject cactus)
    {
         LeanTween.moveLocal(cactus, end.localPosition, timeTodespawn);
         yield return new WaitForSeconds(timeTodespawn);

         GameObject.Destroy(cactus);
         Score++;
         testoScore.text = Score.ToString("d4");

         if (Score > ScoreVictory)
         {
            Victory();
         }
         yield return null;
    }
    void Jump()
    {
        if (!canJump) return;

        canJump = false;
        float tempoSalto = 0.6f;
        LeanTween.moveLocalY(player.gameObject, player.position.y+120f, tempoSalto).setEaseOutSine();
        LeanTween.moveLocalY(player.gameObject, player.position.y, tempoSalto).setDelay(tempoSalto).setEaseInSine();
        LeanTween.delayedCall(tempoSalto*2, StopJump);
    }
    void StopJump()
    {
        canJump = true;
    }

    public void _Lose()
    {
        // Debug.Log("perso dinousaro");
        // testoScore.text = "LOST";
        // isLose = true;

        if (!isLose)
        {
            isLose = true;
        }
    }

    IEnumerator Losing()
    {
        while (!isLose)
        {
            yield return new WaitForSecondsRealtime(1f);
        }

        if (isLose == true)
        {
            Lose();
            yield return null;
        }
    }
}
