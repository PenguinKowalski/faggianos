﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Particolare : MonoBehaviour
{
    public bool Permanente;
    private Image PartImage;
    void Awake()
    {
        int r = Random.Range(0,2);
        Permanente = (r<1) ? true : false;
        PartImage = this.GetComponent<Image>();
    }

    public void OnClick() 
    {
        PartImage.color = Color.red;
        if (!Permanente)
            this.gameObject.SetActive(false);
    }
}
