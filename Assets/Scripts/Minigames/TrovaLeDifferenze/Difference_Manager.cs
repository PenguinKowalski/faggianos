﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Difference_Manager : Minigame
{
    public GameObject[] Particolari;
    public TMP_InputField CampoInput;
    public int NParticolari;
    
    private void Start()
    {
        for(int i=0; i < Particolari.Length; i++) 
        {
            if (Particolari[i].GetComponent<Particolare>().Permanente)
                NParticolari++;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!string.IsNullOrEmpty(CampoInput.text))
            {
                CheckAnswer(int.Parse(CampoInput.text));
            }
        }
          
        
        
    }

    void CheckAnswer(int Ninput) 
    { 
     if(Ninput== NParticolari) 
        {
            Victory();
        }else 
        {
            Lose();
        }
    }
    public Difference_Manager()
    {
    }
}
