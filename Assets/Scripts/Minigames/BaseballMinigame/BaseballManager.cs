﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BaseballManager : Minigame
{
    Animator animator;

    public Slider slider;
    public float speed = 4f;
    public RangeValoriBaseball[] rangeValori;

    public int vite = 3;
    int counter = 0;
    public GameObject[] vitePalle;

    public Button bottone;
    float time=0f;
    bool sliderattivo=true;

    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    void LoopSlider()
    {
        
    }

    public void LaunchButtonOnClick()
    {
        float valore = slider.value;
        int potenzaResult = 0;

        vite--;
        GameObject.Destroy(vitePalle[vite]);

        for (int i=0; i< rangeValori.Length; i++)
        {
            if (valore>=rangeValori[i].min && valore <= rangeValori[i].max)
            {
                potenzaResult = rangeValori[i].potenza;
                break;
            }
        }
        StartCoroutine(DisabilitoBottone());
        if (potenzaResult == 0)
        {
            //CASO FAIL
            //vite--;
            if (vite <= 0)
            {
                Lose();
            }
            else
            {
                animator.Play("Fail");
                //GameObject.Destroy(vitePalle[vite]);
            }
          
        }
        else if (potenzaResult == 1)
        {
            counter++;
            animator.Play("Hit");
            if (counter == 2)
                Victory();
        }
        else
        {
            animator.Play("Victory");
            Victory();
        }
    }

    void Update()
    {
        if (sliderattivo)
        {
            time += Time.deltaTime;
            slider.value = Mathf.PingPong(time * speed, 100);
        }
       
    }

    IEnumerator DisabilitoBottone()
    {
        bottone.enabled = false;
        sliderattivo = false;
        animator.Play(null);
        yield return new WaitForSeconds(2f);
        bottone.enabled = true;
        sliderattivo = true;
        slider.value = 0;
        time = 0;
        yield return null;
    }
}
[System.Serializable]
public class RangeValoriBaseball
{
    public float min, max;
    public int potenza;
}

