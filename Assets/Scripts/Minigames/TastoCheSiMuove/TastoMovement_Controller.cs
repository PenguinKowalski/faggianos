﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TastoMovement_Controller : Minigame
{
    public GameObject Compra_button;
    public Vector3 dir;
    public float speed = 5f;
    public float maxDistance;

    public GameObject GameScreen, MainScreen;

    public float height, width;
    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 rectPos;
    RectTransform rect;

    public Vector2 buttonSize;
    private void Start()
    {
        rect = Compra_button.GetComponent<RectTransform>();

        buttonSize = rect.sizeDelta * 0.5f;

        height = GameScreen.GetComponent<RectTransform>().sizeDelta.y;
        width = GameScreen.GetComponent<RectTransform>().sizeDelta.x;

        borderDown = (GameScreen.transform.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (GameScreen.transform.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (GameScreen.transform.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (GameScreen.transform.position + new Vector3(+width / 2f, 0, 0)).x;
    }
    void Update()
    {
        rectPos = rect.anchoredPosition;
        ClampToWindow();
        MuoviLontano();
    }

    public void MuoviLontano()
    {
        dir = Compra_button.transform.position - VirtualCursor.instance.transform.position;
        dir.z = 0;

        if (dir.magnitude < maxDistance)
        {
          rect.position += dir.normalized * speed * Time.deltaTime;
        }
    }

    void ClampToWindow()
    {
        Vector3 pos = rect.localPosition;

        pos.x = Mathf.Clamp(rect.localPosition.x, borderLeft + buttonSize.x, borderRight - buttonSize.x);
        pos.y = Mathf.Clamp(rect.localPosition.y, borderDown + buttonSize.y, borderUp - buttonSize.y);

        rect.localPosition = pos;
    }

    public void CompraOnClick()
    {
        Victory();
    }

    public void CambiaScenaOnClick()
    {
        MainScreen.SetActive(false);
        GameScreen.SetActive(true);
    }
}
