﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Help_controller : MonoBehaviour
{
    public TextMeshProUGUI avviso_quote;
    public GameObject avviso;
    public int damage = 0;

    public void HelpOnClick()
    {
        damage++;
        Debug.Log(damage);
        FadeOff();
        StartCoroutine(DoT());
    }

    IEnumerator DoT()
    {
        while (true)
        {
            GameManager.Instance.ChangeMoney(-damage);
            yield return new WaitForSeconds(1f);
        }
    }

    public void FadeOff()
    {
        GameObject go = Instantiate(avviso, this.transform);

        Image alpha = go.gameObject.GetComponent<Image>();
        avviso_quote = go.GetComponentInChildren<TextMeshProUGUI>();

        alpha.CrossFadeAlpha(0, 3f, true);
        avviso_quote.CrossFadeAlpha(0, 3f, true);
    }
}
