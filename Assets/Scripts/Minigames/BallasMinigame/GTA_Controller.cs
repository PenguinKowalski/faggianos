﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GTA_Controller : Minigame
{
    public Image[] inputfield;
    public int index = 0;

    public string code;

    
    public void VerifyCode()
    {
        if (string.Equals(code, GameManager.Instance.GTACod))
        {
            Victory();
            Debug.Log("Codice corretto");
        }
        else
        {
            Lose();
            Debug.Log("Codice sbagliato");
        }
    }
    
}

