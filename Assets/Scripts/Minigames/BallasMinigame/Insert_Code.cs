﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Insert_Code : MonoBehaviour
{
    public Image immagine_bottone;      //immagine del tasto premuto
    private Sprite sprite_tasto;        //sprite dell'immagine
    public GTA_Controller controller;
    public int val;

    void Start()
    {
        sprite_tasto = immagine_bottone.gameObject.GetComponent<Image>().sprite;
    }

    public void OnClick()
    {
        if (controller.index <= 6)
            controller.VerifyCode();
     

        if (controller.inputfield[controller.index].gameObject.GetComponent<Image>().sprite != sprite_tasto)
        {
            controller.inputfield[controller.index].gameObject.GetComponent<Image>().sprite = sprite_tasto;
            controller.index++;
            controller.code += val.ToString();
        }
    }
}
