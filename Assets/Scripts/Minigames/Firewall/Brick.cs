﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour,IVirtualClick
{
    public Transform BrickStartPos;    
    private bool MouseDrag = false;
    private Vector3 screenPoint, offset;
    private Camera Camerax;
    private Rigidbody2D BrickRB;
    private float force=5f;
    Vector3 startCursorPos;
    public void Start()
    {
        //BrickStartPos = this.transform;
        Camerax = Camera.main;
        BrickRB = this.GetComponent<Rigidbody2D>();
        //offset = Vector3.zero;
    }

    public void FixedUpdate()
    {
        if (MouseDrag) 
        {
            /*BrickRB.freezeRotation = true;
            Vector3 curScreenPoint = new Vector3(VirtualCursor.instance.CursorPosition.x, VirtualCursor.instance.CursorPosition.y, screenPoint.z);
            Vector3 curPosition = Camera.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;*/
            Vector3 target = Camerax.ScreenToWorldPoint(VirtualCursor.instance.CursorPosition);

            Vector3 posA;
            posA = VirtualCursor.instance.CursorPosition;

            //Debug.Log(posA + "-" );

            Vector3 direction = (posA - startCursorPos).normalized;

           // Debug.Log(direction);
            BrickRB.AddForce(direction * Time.fixedDeltaTime * force, ForceMode2D.Impulse);
            startCursorPos = posA;

        }     

        
    }
    public void Update()
    {
        if (MouseDrag)
        {
            if (Input.GetMouseButtonUp(0))
            {

                OnMouseClickUp(Vector2.zero);
                MouseDrag = false;
            }
        }



    }

    /*void OnMouseUp()
    { if (Mathf.Abs(this.transform.localPosition.x - BrickStartPos.localPosition.x) < snapDistance && Mathf.Abs(this.transform.localPosition.y - BrickStartPos.localPosition.y) < snapDistance)
        { 
            transform.position = BrickStartPos.position;
            transform.rotation = BrickStartPos.rotation;
        }
        
    }*/
    /*void OnMouseDrag()
    {
        this.gameObject.transform.rotation = BrickStartPos.transform.rotation;
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
        
        
    }*/

    public void OnMouseClickDown(Vector2 clickpos)
    {
        Debug.Log("cliccato" + this.name);
        screenPoint = Camerax.WorldToScreenPoint(transform.position);
        offset = this.transform.position - Camerax.ScreenToWorldPoint(new Vector3(VirtualCursor.instance.CursorPosition.x, VirtualCursor.instance.CursorPosition.y, screenPoint.z));
        MouseDrag = true;
        startCursorPos = VirtualCursor.instance.CursorPosition;

    }

    public void OnMouseClickUp(Vector2 clickpos)
    {
        MouseDrag = false;
        BrickRB.freezeRotation = false;
    }
}
