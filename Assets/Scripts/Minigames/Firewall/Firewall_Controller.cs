﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firewall_Controller : MonoBehaviour
{
    public Transform PhisicRoot, Window;
    public GameObject FirewallRoot;
    void OnEnable()
    {
        PhisicRoot.parent = null;
        PhisicRoot.position = Vector3.zero;
        PhisicRoot.localScale = Vector3.one;
        Window.SetParent(FirewallRoot.transform.parent);
        Window.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        Window.localScale = Vector3.one;



        Destroy(FirewallRoot);
        //Destroy(PhisicRoot);
    }
}
