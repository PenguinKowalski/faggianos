﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour    
{
    
   
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ProtectZone") 
        {
            Debug.Log("Malus");
            Destroy(this.gameObject);
            if (SpawnVirus.instance)
                SpawnVirus.instance.Spawn_Virus();
            else
            {
                GameManager.Instance.GiveState(false);
            }
        }   
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Border")
        {
            Destroy(this.gameObject);
        }
    }
}
