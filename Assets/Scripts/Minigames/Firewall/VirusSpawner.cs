﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusSpawner : MonoBehaviour
{
    public Transform VirSpawn;
    public GameObject VirPref;
    void Update()
    {
      if (Input.GetKeyDown(KeyCode.Space)) 
        {
            SpawnVirus(VirSpawn); 
        }   
    }

    public void Spawn()
    {
        SpawnVirus(VirSpawn);
    }
    void SpawnVirus(Transform Spawn) 
    {
        Instantiate(VirPref, Spawn);
    }
}
