﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notifications : Minigame
    
{
    public void ButtonYesOnClick() 
    {
        Lose();
    }
    public void ButtonNOnClick()
    {
        Lose();
    }
    public void ButtonWinOnClick()
    {
        Victory();
    }
}
