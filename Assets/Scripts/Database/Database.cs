﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


[System.Serializable]
public class Database : MonoBehaviour
{ 
        string path;
        string version;
        public AccountSlot[] accounts;
        public bool GameOverFlag = false;

        public int ActualSlot = -1;

        
        public static Database CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Database>(jsonString);
        }

        public bool FromJson(string jsonString)
        {
            JsonUtility.FromJsonOverwrite(jsonString, this);


            return true;
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        void Awake()
        {
            path = Application.persistentDataPath + "/saves.FAGGIAN";

            if (File.Exists(path))
            {
                Load();
            }
            else
            {
                Save();
            }
        }

        public void Save()
        {
            WriteString(path, ToJson());
            Debug.Log("saved!");
        }
        public void Load()
        {
            FromJson(ReadString(path));
            Debug.Log("loaded!");
        }


        static void WriteString(string path, string text)
        {
            StreamWriter writer = new StreamWriter(path, false);
            writer.WriteLine(text);
            writer.Close();
        }

        static string ReadString(string path)
        {
            StreamReader reader = new StreamReader(path);
            string text = reader.ReadToEnd();

            reader.Close();
            return text;
        }
        public bool ChangeZucchini(int qntZucchini)
        {
            if (accounts[ActualSlot].zucchini + qntZucchini < 0) return false;

            accounts[ActualSlot].zucchini += qntZucchini;

            if (qntZucchini > 0)
            {
                GameManager.Instance.winZucchine_ps.Play();
            }
            else
            {
                GameManager.Instance.loseZucchine_ps.Play();
            }
            GameManager.Instance.desktop.UpdateMoney();
        return true;


        }

        public int GetZucchini()
        {
            return accounts[ActualSlot].zucchini;
        }
        
        public void BuyBonus(int bonus)
        {
            if (accounts[ActualSlot].listBonusBought == null)
            {
                accounts[ActualSlot].listBonusBought = new List<int>();
            }
            if (!accounts[ActualSlot].listBonusBought.Contains(bonus))
            {
                accounts[ActualSlot].listBonusBought.Add(bonus);
            }
            Save();

        }

        public bool IsBonusBought(int bonus)
        {
            if (accounts[ActualSlot].listBonusBought == null) return false;

            return (accounts[ActualSlot].listBonusBought.Contains(bonus));
        }
    }

[System.Serializable]
public class AccountSlot
{
    public string name;
    public Sprite immagine;
    public int zucchini;
    public List<int> listBonusBought;
}