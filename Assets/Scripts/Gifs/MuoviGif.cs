﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class MuoviGif : MonoBehaviour
{
    public List<Transform> gif;
    public int n;
    public Vector3 dir;
    public float speed = 1f;

    public float height, width;

    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 buttonSize;


    // Start is called before the first frame update
    void Start()
    {
        n = Random.Range(0, 16);

        dir = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
        
    }

    // Update is called once per frame
    void Update()
    {
        gif[n].transform.localPosition += speed * dir;


        borderDown = (this.transform.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (this.transform.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (this.transform.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (this.transform.position + new Vector3(+width / 2f, 0, 0)).x;

           
     
            if (gif[n].transform.localPosition.y + buttonSize.y > borderUp)
            {
                dir = new Vector3(dir.x, -dir.y, dir.z);
            }
            if (gif[n].transform.localPosition.y - buttonSize.y < borderDown)
            {
                dir = new Vector3(dir.x, -dir.y, dir.z);
            }
            if (gif[n].transform.localPosition.x + buttonSize.x > borderRight)
            {
                dir = new Vector3(-dir.x, dir.y, dir.z);
            }
            if (gif[n].transform.localPosition.x - buttonSize.x < borderLeft)
            {
                dir = new Vector3(-dir.x, dir.y, dir.z);
            }
           



    }
}