﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FintoSpegnimento : Minigame
{
    public GameObject notifica, spegnimento;
    public Transform posNotifica, posSpegnimento;
    public int timerNotifica = 10;
    public ScriptableObject managerButton;

    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(Notifica());
    }

    IEnumerator Notifica()
    {
        GameObject go= GameObject.Instantiate(notifica, posNotifica);
        yield return new WaitForSeconds(timerNotifica);
        GameObject.Destroy(go);
        GameObject go2 = GameObject.Instantiate(spegnimento, posSpegnimento);
        ManagerButton bt = go2.GetComponentInChildren<ManagerButton>();
        bt.TrollFaceButton.onClick.AddListener(ClickTrollFace);
        bt.NextButton.onClick.AddListener(ClickNext);
        bt.BackButton.onClick.AddListener(ClickBack);
        bt.CloseButton.onClick.AddListener(ClickClose);
    }
    public void ClickTrollFace()
    {
        Victory();
        Debug.Log("win");
    }
    public void ClickNext()
    {
        Debug.Log("lose");
        Lose();
        
    }
    public void ClickClose()
    {
        Lose();
        Debug.Log("lose");
    }
    public void ClickBack()
    {
        Lose();
        Debug.Log("lose");
    }

}
