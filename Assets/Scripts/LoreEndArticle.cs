﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoreEndArticle : Minigame
{
    public void Start()
    {
        finestraRef.closeButton.onClick.AddListener(CloseWindow);
    }

    public void CloseWindow() 
    {
        Close();
    }
}
