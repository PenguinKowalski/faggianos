﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SidequestPapa_controller : State
{
    public GameObject Papa_neutral, Papa_evil, Papa_saint;
    public GameObject start_quote,change_quote, addios_quote;
    public GameObject sideBarPope, sideBarPope_prefab;
    public TextMeshProUGUI debole;

    public bool mission_accepted = false;

    public static SidequestPapa_controller instance;

    private void Awake()
    {
        instance = this;
    }
    public void ContinueOnClick()
    {
        start_quote.SetActive(false);
        change_quote.SetActive(true);
    }

    public void AccettaOnCLick()
    {
        sideBarPope_prefab = Instantiate(sideBarPope, GameManager.Instance.InterfaceRoot.transform);
        mission_accepted = true;
        //GameManager.Instance.StartPapaCounter();       toDo
        this.gameObject.SetActive(false);
    }

    public void RifiutaOnClick()
    {
        change_quote.SetActive(false);
        addios_quote.SetActive(true);
        FadeTo();
        StartCoroutine(CloseThis());
    }

    public void FadeTo()
    {
        Image alpha = Papa_neutral.gameObject.GetComponent<Image>();
        Image beta = addios_quote.gameObject.GetComponent<Image>();
    
        debole.CrossFadeAlpha(0, 2f, true);
        alpha.CrossFadeAlpha(0, 2f, true);
        beta.CrossFadeAlpha(0, 2f, true);
    }

    IEnumerator CloseThis()
    {
        yield return new WaitForSecondsRealtime(2f);
        this.gameObject.SetActive(false);

        yield return null;
    }
}
