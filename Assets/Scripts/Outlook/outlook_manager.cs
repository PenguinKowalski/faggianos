﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class outlook_manager : Minigame
{
    private int mail_arrived=0;
    public int max_mail;
    public float min_time, max_time;
    public string reply;
    public bool is_writing = false;
    public int index=0;


    public VerticalLayoutGroup layout_group;

    public GameObject mail_prefab;
    private List<GameObject> mails = new List<GameObject>();
    public GameObject mail_conteiner;
    public GameObject reply_panel;

    public TextMeshProUGUI incoming_mail_text,opened_mail_text;
    public TextMeshProUGUI from_text, to_text;

    public Mail openedMail;

    float t=0;
    enum Mailstate {sent, received,none };
    Mailstate stato = Mailstate.none;
    void Start()
    {
        finestraRef.closeButton.onClick.AddListener(CloseWindow);
        //StartCoroutine(SpawnMail(debug_mail));
        ClearMailList(true);

       
    }

    void Update()
    {
        if (is_writing)
        {
            if(Input.anyKeyDown)
            {
                if (index >= reply.Length)
                {
                    is_writing = false;
                    //invio mail risposta
                    for(int i=0;i< openedMail.NextMail.Length; i++)
                    {
                        GameManager.Instance.ListaMail.Add(openedMail.NextMail[i]);
                    }
                    GameManager.Instance.ListaMailInUscita.Add(openedMail.Cod);
                    OpenSentMails_OnClick(true);
                }
                else
                {
                    opened_mail_text.text += reply[index];
                    index++;
                }
            }
        }

        if (!string.IsNullOrEmpty(GameManager.Instance.OutlookCod))
        {
            t += Time.deltaTime;
            if (t > 1f)
            {
                t = 0;
                Refresh(false);
            }
        }
    }
    /*
    IEnumerator SpawnMail(Mail datamail)
    {
        for(int i=0;i< max_mail;i++)
        {
            mail_test = Instantiate(mail_prefab, mail_conteiner.transform);
            mail_test.transform.SetParent(mail_conteiner.transform, false);
            mail_button _button = mail_test.GetComponent<mail_button>();
            _button.data_mail = datamail;
            _button._from.text = datamail.Indirizzo;
            _button._object.text = datamail.Oggetto;
            mail_arrived++;
            incoming_mail_text.text = " INCOMING MAIL " + "(" + mail_arrived.ToString() + ")";
            yield return new WaitForSeconds(Random.Range(min_time,max_time));
        }
    }
    */
    public void open_list_OnClick()
    {
       
    }

    public void Refresh(bool removemailopened)
    {
        if (stato == Mailstate.received)
        {
            OpenReceivedMails_OnClick(removemailopened);
        }
        else if (stato == Mailstate.sent)
        {
            OpenSentMails_OnClick(removemailopened);
        }
    }

    void ClearMailList(bool clearmailopened)
    {
        foreach(GameObject go in mails)
        {
            Destroy(go);
        }
        mails.Clear();

        if (clearmailopened)
        {
            opened_mail_text.text = "";
            from_text.text = "From:";
            to_text.text = "To: You";
            reply_panel.SetActive(false);
            openedMail = null;
        }
      
    }

    public void ClearOpenedMail()
    {
        Refresh(true);
        opened_mail_text.text = "";
        from_text.text = "From:";
        to_text.text = "To: You";
        reply_panel.SetActive(false);
        openedMail = null;
    }
    public void OpenSentMails_OnClick(bool resetopenedmail)
    {
        stato = Mailstate.sent;
        ClearMailList(resetopenedmail);

        for (int i = 0; i < GameManager.Instance.ListaMailInUscita.Count; i++)
        {
            GameObject mail_go = Instantiate(mail_prefab, mail_conteiner.transform);
            Mail mail = GameManager.Instance.MailHash[GameManager.Instance.ListaMailInUscita[i]];
            mails.Add(mail_go);
            mail_go.transform.SetParent(mail_conteiner.transform, false);
            mail_button _button = mail_go.GetComponent<mail_button>();
            _button.data_mail = mail;
            _button._from.text = mail.Indirizzo;
            _button._object.text = "RE:" + mail.Oggetto;
            _button.isSent = true;
            // mail_arrived++;
            // incoming_mail_text.text = " INCOMING MAIL " + "(" + mail_arrived.ToString() + ")";
        }

    }
 
    public void OpenReceivedMails_OnClick(bool resetopenedmail)
    {
        stato = Mailstate.received;
        ClearMailList(resetopenedmail);
        int counttoread = 0;

        for (int i = 0; i < GameManager.Instance.ListaMailInArrivo.Count; i++)
        {
            GameObject mail_go = Instantiate(mail_prefab, mail_conteiner.transform);
            Mail mail = GameManager.Instance.MailHash[GameManager.Instance.ListaMailInArrivo[i]];
            mails.Add(mail_go);
            mail_go.transform.SetParent(mail_conteiner.transform, false);
            mail_button _button = mail_go.GetComponent<mail_button>();
            _button.data_mail = mail;
            _button._from.text = mail.Indirizzo;
            _button._object.text = mail.Oggetto;
            _button.isSent = false;

            if (!GameManager.Instance.MailDataHash[mail].isRead)
            {
                _button._from.fontStyle = FontStyles.Bold;

                _button._object.fontStyle = FontStyles.Bold;

                counttoread++;
            }
            // mail_arrived++;
            // incoming_mail_text.text = " INCOMING MAIL " + "(" + mail_arrived.ToString() + ")";
        }

        if (counttoread==0)
            incoming_mail_text.text = "INCOMING MAIL";
        else
        {
            incoming_mail_text.text = "INCOMING MAIL (" + counttoread+")";
        }
    }
    public void OpenSentMail(Mail mail)
    {
        from_text.text = "From: You";
        to_text.text = "To:" + mail.Indirizzo;


        string text = mail.Risposta[GameManager.Instance.MailDataHash[mail].selectAnswerIndex];

        string[] textsplits = text.Split('$');

        for(int i=0; i<textsplits.Length; i++)
        {
            string variable;
            if (GameManager.Instance.MailDynamicVariables.TryGetValue(textsplits[i], out variable)){
                textsplits[i] = variable;
            }        
        }

        text = string.Join("", textsplits);

        opened_mail_text.text = text;
        reply_panel.SetActive(false);


        GameManager.Instance.MailDataHash[mail].isRead  = true;
        openedMail = mail;
        updateCanvas();
    }
    public void OpenMail(Mail mail)
    {
        from_text.text = "From: " + mail.Indirizzo;
        string text = mail.Messaggio;

        string[] textsplits = text.Split('$');

        for (int i = 0; i < textsplits.Length; i++)
        {
            string variable;

            if (GameManager.Instance.MailDynamicVariables.TryGetValue(textsplits[i], out variable))
                {
                Debug.Log(variable);
                textsplits[i] = variable;
            }
        }

        text = string.Join("", textsplits);

        opened_mail_text.text = text;


        if (mail.Risposta.Length==0)
        {
            reply_panel.SetActive(false);
        }
        else
        {
            reply = mail.Risposta[GameManager.Instance.MailDataHash[mail].selectAnswerIndex];
            Debug.Log(reply);
            if (string.IsNullOrEmpty(reply))
            {
                reply_panel.SetActive(false);
            }
            else
            {
                reply_panel.SetActive(true);
            }
        }
      
        GameManager.Instance.MailDataHash[mail].isRead = true;
        openedMail = mail;
        updateCanvas();
    }

    public void updateCanvas()
    {
        Canvas.ForceUpdateCanvases();
        layout_group.SetLayoutVertical();
        layout_group.enabled = false;
        layout_group.enabled = true;
        LayoutRebuilder.ForceRebuildLayoutImmediate(this.GetComponent<RectTransform>());
    }

    public void send_reply_OnClick()
    {
        opened_mail_text.text = "";
        reply_panel.SetActive(false);
        is_writing = true;
        updateCanvas();
    }

    public void CloseWindow()
    {
        finestraRef.Close(-1);
        GameManager.Instance.CloseOutlook();
    }
}
