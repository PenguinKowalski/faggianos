﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System.Linq;

[RequireComponent(typeof(TextMeshProUGUI))]
public class OpenHyperlinks : MonoBehaviour
{
    TextMeshProUGUI pTextMeshPro;
    Camera pCamera;
    outlook_manager outlook;
   // List<Color32[]> oldVertColors = new List<Color32[]>(); // store the old character colors

    public Color hoverColor;
    private void Start()
    {
        pTextMeshPro = this.GetComponent<TextMeshProUGUI>();
        pCamera = Camera.main;
        outlook = FindObjectOfType<outlook_manager>();
    }
    public void OnPointerClick()
    {
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, VirtualCursor.instance.CursorPosition, pCamera);
        if (linkIndex != -1)
        { // was a link clicked?
            TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];
            GameManager.Instance.SendMinigame(outlook.openedMail.MinigameAllegato,outlook.openedMail.FirstMail);
        }
    }
    public int index;
    public void OnPointerEnter()
    {
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(pTextMeshPro, VirtualCursor.instance.CursorPosition, pCamera);
        if (linkIndex != -1)
        { // was a link clicked?
            SetLinkToColor(linkIndex, hoverColor);
            index = linkIndex;
        }
    }
    
    public void OnPointerExit()
    {
        SetLinkToColor(index, Color.white);
    }
    
    List<Color32[]> SetLinkToColor(int linkIndex, Color32 color)
    {
        TMP_LinkInfo linkInfo = pTextMeshPro.textInfo.linkInfo[linkIndex];

        // oldVertColors.Clear();
        List<Color32[]> oldVertColors = new List<Color32[]>(); // store the old character colors

        for (int i = 0; i < linkInfo.linkTextLength; i++)
        { // for each character in the link string
            int characterIndex = linkInfo.linkTextfirstCharacterIndex + i; // the character index into the entire text
            var charInfo = pTextMeshPro.textInfo.characterInfo[characterIndex];
            int meshIndex = charInfo.materialReferenceIndex; // Get the index of the material / sub text object used by this character.
            int vertexIndex = charInfo.vertexIndex; // Get the index of the first vertex of this character.

            Color32[] vertexColors = pTextMeshPro.textInfo.meshInfo[meshIndex].colors32; // the colors for this character
           // oldVertColors.Add(vertexColors.ToArray());

            if (charInfo.isVisible)
            {
                vertexColors[vertexIndex + 0] = color;
                vertexColors[vertexIndex + 1] = color;
                vertexColors[vertexIndex + 2] = color;
                vertexColors[vertexIndex + 3] = color;
            }
        }

        // Update Geometry
        pTextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

        return oldVertColors;
    }

  
}