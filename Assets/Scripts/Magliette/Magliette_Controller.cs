﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magliette_Controller : Minigame
{
    public int X_counter;
    public int Compra_counter;
    public int costo_soldi;

    public Canvas canvas;
    public float height, width;
    public float borderUp, borderDown, borderLeft, borderRight;

    private void Start()
    {
        finestraRef.closeButton.onClick.AddListener(ChiusuraConX);
        canvas = this.gameObject.GetComponentInParent<Canvas>();

        height = canvas.GetComponent<RectTransform>().sizeDelta.y;
        width = canvas.GetComponent<RectTransform>().sizeDelta.x;

        borderDown = (canvas.transform.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (canvas.transform.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (canvas.transform.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (canvas.transform.position + new Vector3(+width / 2f, 0, 0)).x;
    }

    private void Update()
    {
        if (X_counter >= 4 || Compra_counter >= 4)
        {
            CloseWindow();
        }
    }

    public void ChiusuraConX()
    {
        X_counter++;

        if (X_counter > 1)
        {
            finestraRef.gameObject.transform.localPosition = new Vector2(Random.Range(borderLeft/2, borderRight/2), Random.Range(borderDown/2, borderUp/2));
            GameManager.Instance.ChangeMoney(-costo_soldi);
        }
    }

    public void CompraMagliettaOnClick()
    {
        Compra_counter++;

        finestraRef.gameObject.transform.localPosition = new Vector2(Random.Range(borderLeft / 2, borderRight / 2), Random.Range(borderDown / 2, borderUp / 2));
    }

    public void CloseWindow()
    {
        Victory();
    }
}
