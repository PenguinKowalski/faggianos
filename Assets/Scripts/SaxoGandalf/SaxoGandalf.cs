﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaxoGandalf : State
{
    float tempoLimite = 30f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Counter());
    }

    IEnumerator Counter()
    {
        int count = 0;

        while (count < tempoLimite)
        {
            count++;
            Debug.Log(count);
            yield return new WaitForSeconds(1);
        }

        GameManager.Instance.RemoveState("SaxoGandalf");
        yield return null;
    }
}
