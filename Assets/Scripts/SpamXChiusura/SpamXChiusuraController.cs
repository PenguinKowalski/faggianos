﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpamXChiusuraController : Minigame
{
    public float minWindowX = -300f, maxWindowX = 300f, minWindowY = -250f, maxWindowY = 250f;
    public Transform parent;
    // Start is called before the first frame update
    public GameObject XCorretta, XErrata;
    void Start()
    {
        for (int i = 0; i < 9; i++)
        {
            GameObject go = GameObject.Instantiate(XErrata, parent);
            RectTransform rt = go.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(Random.Range(minWindowX, maxWindowX), Random.Range(minWindowY, maxWindowY));
            Button bt = go.GetComponent<Button>();
            bt.onClick.AddListener(LoseOnClick);
        }
        { 
        GameObject go = GameObject.Instantiate(XCorretta, parent);
        RectTransform rt = go.GetComponent<RectTransform>();
        rt.anchoredPosition = new Vector2(Random.Range(minWindowX, maxWindowX), Random.Range(minWindowY, maxWindowY));
            Button bt = go.GetComponent<Button>();
            bt.onClick.AddListener(VictoryOnClick);

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void VictoryOnClick()
    {
        Victory();
    }
    public void LoseOnClick()
    {
        Lose();
    }

}

