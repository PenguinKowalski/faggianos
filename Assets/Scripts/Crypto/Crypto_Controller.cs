﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Crypto_Controller : Minigame
{
    public TextMeshProUGUI Totale_zucchine;
    public int soldi_persi, total;
    public int counter;
    public bool isLose = false;

    private void Start()
    {
        total = 10;

        if (tempoLimite > 0)
        {
            StartCoroutine(CloseDelayedTimer());
        }
        StartCoroutine(Losing());
    }

    void Update()
    {
        Totale_zucchine.text = "Soldi guadagnati: " + soldi_persi;

        if (counter == 0)
        {
            isLose = true;
        }
    }

    public void CryptoSpam()
    {
        soldi_persi += total;
        GameManager.Instance.ChangeMoney(total);
    }

    IEnumerator CloseDelayedTimer()
    {
        counter = (int)tempoLimite;

        while (counter > 0)
        {
            string minutes = Mathf.Floor(counter / 60).ToString("00");
            string seconds = (counter % 60).ToString("00");

            counter--;
            yield return new WaitForSeconds(1);
        }
        yield return null;
    }

    IEnumerator Losing()
    {
        while (!isLose)
        {
            yield return new WaitForSecondsRealtime(1f);
        }

        if (isLose == true)
        {
            GameManager.Instance.ChangeMoney((-soldi_persi)*2);
            Victory();
            yield return null;
        }
    }
}
