﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using antilunchbox;

public class Minigame : MonoBehaviour
{
    public float tempoLimite = 30;
    [Range(0f,1f)]
    public float chanceMalus = 0.9f;
    [Range(0f, 1f)]
    public float chanceBonus = 0.1f;
    public int difficolty = 0;
    public float delayAfterWinLose = 2f;

    public Finestra finestraRef;

    public string Title, Detail;
    public Sprite icona;

    public bool isMain=false;

    public int CostoSconfitta = 500;
    public GameObject SchermataVictory, SchermataLose;
    public virtual void Victory()
    {
        StartCoroutine(CloseDelayed(1));
        if (SchermataVictory)
        SchermataVictory.SetActive(true);
        GameManager.Instance.Reward(chanceBonus);
        SoundManager.PlayCappedSFX(SoundManager.Load("ZucchineUp"), "ZucchineUp");
    }
    public virtual void SpawnFinestra()
    {
        if (tempoLimite > 0)
        {
            StartCoroutine(CloseDelayedTimer());
        }
        else finestraRef.TimerText.text = "";
    }
    public virtual void Lose()
    {
        StartCoroutine(CloseDelayed(0));
        if (SchermataLose)
        SchermataLose.SetActive(true);
        GameManager.Instance.ChangeMoney(-CostoSconfitta);
        GameManager.Instance.Punish(chanceMalus);
        SoundManager.PlayCappedSFX(SoundManager.Load("ZucchineDown"), "ZucchineDown");
    }
    public virtual void Close()
    {
        StartCoroutine(CloseDelayed(-1));    
    }

    IEnumerator CloseDelayedTimer()
    {
        int counter = (int)tempoLimite;

        while (counter > 0)
        {          
            string minutes = Mathf.Floor(counter / 60).ToString("00");
            string seconds = (counter % 60).ToString("00");

            finestraRef.TimerText.text = minutes + ":" + seconds;
            counter--;
            yield return new WaitForSeconds(1);
        }
        Lose();
        yield return null;
    }

    protected IEnumerator CloseDelayed(int winlose)
    {
        yield return new WaitForSeconds(delayAfterWinLose);    
       
        if (finestraRef)
            finestraRef.Close(winlose);
        else
        {
            Debug.Log("destroy window");
            Destroy(this.gameObject);

        }

        if (isMain)
        {
            Debug.Log("send another minigame");
            GameManager.Instance.SendMail(UnityEngine.Random.Range(0, 4));
            GameManager.Instance.SendMinigame(-1, isMain,10f);
        }
        yield return null;
    }


}

[Serializable]
public class MinigameData
{
    public GameObject prefab;
    public int rarity;
    public float height;
    public bool isHalf;
    public bool createWindow = true;
}
