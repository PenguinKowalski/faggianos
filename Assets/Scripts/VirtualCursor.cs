﻿
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class VirtualCursor : UIBehaviour
{
    public Camera cam;
    public static VirtualCursor instance;

    public Sprite[] listTextures;

    public string HorizontalAxis = "Mouse X";
    public string VerticalAxis = "Mouse Y";

    public float StartSensitivity;
    public float Sensitivity;
    public bool EnableOnStart;

    private RectTransform _rectTransform;
    public int cursorIndex = 0;
    public bool Enabled { get; private set; }
    public Image cursor;

    public bool m_lazy = false;
    public bool m_chaos = false;

    ParticleSystem sparkles;

    public Vector2 CursorPosition;

    public bool isInverted;

    protected override void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        instance = this;
        cursor = this.GetComponent<Image>();

        Cursor.visible = false;

#if UNITY_STANDALONE_WIN
      Cursor.lockState = CursorLockMode.Locked;
      Debug.Log("Stand Alone Windows");
#endif


    }
    public void MakeGlow()
    {
        sparkles.Play();
    }
    public Vector2 mousePos()
    {
       return  cam.WorldToScreenPoint(mousePosWorld());
       // return _rectTransform.anchoredPosition;
    }
    public Vector3 mousePosWorld()
    {
        return _rectTransform.transform.position;
      
        
    }
    public Ray CamToCursorRay()
    {
        Ray ray;
        Vector3 input = mousePosWorld();

        ray = new Ray(cam.transform.position, (input - cam.transform.position));
        return ray;
    }
    public bool isAiming()
    {
        if (cursorIndex == 3) return true;
        return false;
    }
    public void SetCursor(int cursorNumber)
    {
        cursor.sprite = listTextures[cursorNumber];
        cursorIndex = cursorNumber;

    }
    protected override void Start()
    {
        isInverted = false;
        if (EnableOnStart)
        {
            Enable();
          
        }
    }

    public void Enable()
    {
        
        StartSensitivity = Sensitivity;
        LockCursor();
        Enabled = true;
    }

    public void Disable()
    {
        UnlockCursor();
        Enabled = false;
    }

    void LockCursor()
    {
        if (Cursor.lockState == CursorLockMode.None)
        {
           // Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void UnlockCursor()
    {
        if (Cursor.lockState == CursorLockMode.Locked)
        {
           // Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void Update()
    {
        if (Enabled)
        {
            if (m_chaos)
            {
                if (chaos == null) chaos= StartCoroutine(ChaosCursor());
            }
            else
            {
                if (chaos != null) StopCoroutine(chaos);
            }

            if (m_lazy)
            {
                if (lazy == null) lazy = StartCoroutine(LazyCursor());
            }
            else
            {
                if (lazy != null) StopCoroutine(lazy);
            }

            MovePointer();
            if (isInverted==true)
            {
                HorizontalAxis = "Mouse Y";
                VerticalAxis = "Mouse X";
            }
            if (isInverted == false)
            {
                HorizontalAxis = "Mouse X";
                VerticalAxis = "Mouse Y";
            }

            if (EventSystem.current.IsPointerOverGameObject())
            {               
                if (Input.GetMouseButton(0))
                {
                    SetCursor(2);
                }
                else
                {
                    SetCursor(1);
                }
            }
            else
            {
                SetCursor(0);
            }
        }
    }
    Coroutine lazy;
    Coroutine chaos;
    public float speed = 0.1f;
    public float range = 0.1f;
    IEnumerator LazyCursor()
    {
        while (true)
        {
            Sensitivity = Random.Range(0, StartSensitivity * 2);
            yield return new WaitForSeconds(speed);          
        }      
    }
    IEnumerator ChaosCursor()
    {
        while (true)
        {
            Vector2 random = Random.insideUnitCircle* range;
            this.transform.position = new Vector3(this.transform.position.x + random.x, this.transform.position.y+random.y, this.transform.position.z);
            yield return new WaitForSeconds(speed);         
        }
    }

    private void MovePointer()
    {
        if (_rectTransform == null)
        {
            return;
        }

        var movement = new Vector2(Input.GetAxis(HorizontalAxis), Input.GetAxis(VerticalAxis)) * Sensitivity;
        
        CursorPosition = Clamp(_rectTransform.anchoredPosition + movement);
        _rectTransform.anchoredPosition = CursorPosition;

        CursorPosition =  RectTransformUtility.WorldToScreenPoint(cam,_rectTransform.position);

    }

    public void IngrandisciMouse()
    {
        this.transform.localScale *= 2f;
    }

    public IEnumerator MouseDisappear()
    {
        this.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(5f);
        this.gameObject.SetActive(true);
        yield return null;
    }

    private Vector2 Clamp(Vector2 pos)
    {
        float ScreenX = (Screen.width / 2);
        float ScreenY = (Screen.height / 2)*1.2f;
     //   _canvasRect = LimitCanvas.rect;
        pos.x = Mathf.Clamp(pos.x, -ScreenX, ScreenX);
        pos.y = Mathf.Clamp(pos.y, -ScreenY, ScreenY);
        return pos;
    }

    public static Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
        rect.x -= (transform.pivot.x * size.x);
        rect.y -= ((1.0f - transform.pivot.y) * size.y);
        return rect;
    }
}