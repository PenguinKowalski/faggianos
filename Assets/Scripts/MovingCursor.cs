﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCursor : MonoBehaviour
{

    private Vector2 m_LastMousePosition;
    private Vector2 m_MousePosition;

    public Vector3 Direction;

    public Transform cursore;
    // Start is called before the first frame update
    void Start()
    {
        m_LastMousePosition = Input.mousePosition;
        //Set Cursor to not be visible
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    // Update is called once per frame
    void Update()
    {
        m_MousePosition = Input.mousePosition;

        Direction = m_MousePosition - m_LastMousePosition;

        m_LastMousePosition  = m_MousePosition ;


        cursore.position = Direction + cursore.position;
    }
}
