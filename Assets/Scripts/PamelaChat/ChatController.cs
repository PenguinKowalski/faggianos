﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChatController : MonoBehaviour
{
    public string[] PaMessages, UrMessages;
    private int MessageIndex;
    public int MessageStartIndex, PictureSwitchIndex; 
    public float ResponseDelay;
    public TextMeshProUGUI PaText, UrText;
    public TMP_InputField Inputfield;
    private string UrWhiteString;
    public GameObject GoodProfile, BadProfile;
    public PamelaChat Controller;
    private bool IsTyping;
    
     void Start()
    {
        MessageIndex = MessageStartIndex;
        PaText.text = PaMessages[MessageIndex];
        IsTyping = false;
        UrWhiteString = "<color=white>Your Reply Here!</color>";
        BadProfile.SetActive(false);
        GoodProfile.SetActive(true);
    }

    
    void Update()
    {
        if (Input.anyKeyDown&& IsTyping) 
        {           
            if (MessageIndex<UrMessages.Length)
            {
                if (Inputfield.text.Length < UrMessages[MessageIndex].Length && !Input.GetMouseButtonDown(0))
                {
                    string ActualResponse = UrMessages[MessageIndex];
                    char[] ActRespToChar = UrMessages[MessageIndex].ToCharArray();
                    Inputfield.text += ActRespToChar[Inputfield.text.Length];
                }
            }
               
            
        }
        if (MessageIndex > PictureSwitchIndex) 
        {
            BadProfile.SetActive(true);
            GoodProfile.SetActive(false);
        }
        
        
    }
    public void SendReply() 
    {
        if (Inputfield.text.Length == UrMessages[MessageIndex].Length)
            StartCoroutine(ReplyCO());
            
    }
    public void StartType() { IsTyping = true; }
    public void StopType() { IsTyping = false; }
    public IEnumerator ReplyCO() 
    {
        UrText.text = UrMessages[MessageIndex];
        Inputfield.text = "";
        MessageIndex++;        
        if (MessageIndex == PaMessages.Length)
        {
            Controller.Close();
        }
        yield return new WaitForSeconds(ResponseDelay);
        if (PaMessages.Length >= MessageIndex) yield return null;
        PaText.text = PaMessages[MessageIndex];
        UrText.text = UrWhiteString;
        yield return null;
    }
}
