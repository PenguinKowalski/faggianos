﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GuariscimiGesu : State
{
    public VideoClip Jesoo;
    float tempoLimite;

    // Start is called before the first frame update
    void Start()
    {
        tempoLimite = (float)Jesoo.length;
        StartCoroutine(Counter());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator Counter()
    {
        int count = 0;
        while (count < tempoLimite)
        {
            count++;
            yield return new WaitForSeconds(1);
        }
        GameManager.Instance.RemoveState("GuariscimiGesù");

        yield return null;
    }
}


