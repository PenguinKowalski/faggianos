﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotificationSpammer : State
{
    private RectTransform SpawnRectTransform;
    public string[] TestiNotifiche;
    public GameObject NotificaPref;
    public float TimeInterval;
    private float Timer;
    
    public NotificationSpammer()
    {
    }
    private void Start()
    {
        SpawnRectTransform = GetComponent<RectTransform>();
        SpawnRectTransform.localPosition = new Vector3(728,-322,0);
        Timer = TimeInterval;
        SpawnNotifica();
        
    }
    private void Update()
    {
        Timer -= Time.deltaTime;
        if (Timer <= 0) 
        {
            Timer = TimeInterval;
            SpawnNotifica();
        }
    }
    public void SpawnNotifica() 
    {
        GameObject Notifica = Instantiate(NotificaPref, this.transform.position, Quaternion.identity, this.transform.parent);
        
        TextMeshProUGUI NotifText = Notifica.GetComponentInChildren<TextMeshProUGUI>();
        NotifText.text = TestiNotifiche[Random.Range(0, TestiNotifiche.Length)];
        
    }
}
