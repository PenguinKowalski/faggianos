﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificaLifeSpan : State
{
    private float Timer;
    public float Lifetime;
    void Start()
    {
        Timer = Lifetime;
    }

    
    void Update()
    {
        Timer -= Time.deltaTime;
        if (Timer <= 0)
            Destroy(this.gameObject);
    }
}
