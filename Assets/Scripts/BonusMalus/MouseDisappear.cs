﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDisappear : State
{
    private void Start()
    {
       StartCoroutine(VirtualCursor.instance.MouseDisappear());
    }
}
