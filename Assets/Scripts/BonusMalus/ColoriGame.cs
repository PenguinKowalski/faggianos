﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ColoriGame : State
{
    VolumeProfile profile;
    ColorAdjustments color;

    public float durata_malus = 10f;

    private void Start()
    {
        profile = this.GetComponent<Volume>().profile;
        profile.TryGet(out color);

        StartCoroutine(ChangeAllColor());
    }

    IEnumerator ChangeAllColor()
    {
       while (durata_malus > 0)
       {
         color.hueShift.Override(Random.Range(0f, 100f));
         durata_malus--;
         yield return new WaitForSeconds(Random.Range(0.5f, 3f));
       }
        yield return null;
    }
}
