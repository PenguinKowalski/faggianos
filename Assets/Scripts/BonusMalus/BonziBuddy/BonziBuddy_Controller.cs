﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

namespace BonziBuddy
{

    public class BonziBuddy_Controller : State,iMinigameEvent
    {
        private Transform BonziTransform;
        private bool IsDragging,IsOver;
        public GameObject SpeechBubble, Menu;
        public GameObject DownloadMalus, BonziPrefab;
        public List<BonziData> BonziDatalist;
        public List<string> BonziRandom, BonziJoke, BonziFacts, BonziStory, BonziAudioVideo, BonziSong, BonziListen;
        public TextMeshProUGUI BubbleText;
        public float BubblePersistance, AutoRandSpeechInterval;
        public AudioSource AudioSource;
        private float AutoSpeechTimer;
        private int MaxBuddy;
        Dictionary<string, BonziData> BonziDataFinderDic = new Dictionary<string, BonziData>();

        private void OnEnable()
        {
            GameManager.Instance.AddListenerMinigameEvent(this);
        }
        public void OnDestroy()
        {
            GameManager.Instance.RemoveListenerMinigameEvent(this);
        }
        public BonziBuddy_Controller()
        {

        }

        
        void Start()
        {
            BonziTransform = GetComponent<Transform>();
            IsDragging = false;            
            IsOver = false;
            Menu.SetActive(false);            
            SpeechBubble.SetActive(false);
            AutoSpeechTimer = 0;
            MaxBuddy = 5;
            foreach (BonziData m in BonziDatalist)
                BonziDataFinderDic.Add(m.BonziDataName, m);
        }

        
        void Update()
        {
            AutoSpeechTimer += Time.deltaTime;
            if (IsDragging) 
            {
                this.transform.position = new Vector3(VirtualCursor.instance.transform.position.x, VirtualCursor.instance.transform.position.y, this.transform.position.z);               
            }
            if (Input.GetMouseButtonDown(1)&&IsOver)
            {
                Menu.SetActive((Menu.activeSelf) ? false : true);
            }
            if(AutoSpeechTimer>= AutoRandSpeechInterval) 
            {
                //RandomSpeech();
                FindRandomBDofType(BonziRandom);
                AutoSpeechTimer = 0;
            }
        }

        public IEnumerator SpeakCo(int i) 
        {
           //Debug.Log("SpeakCo" + i);
            SpeechBubble.SetActive(true);
            BubbleText.text = BonziDatalist[i].BonziTextString;
            AudioSource.PlayOneShot(BonziDatalist[i].BonziSpeechClip);
            yield return new WaitForSeconds(BubblePersistance);
            SpeechBubble.SetActive(false);
        }

        public void OnPointerDown()
        {
            IsDragging = true;
        }
        public void OnPointerUp()
        {
            IsDragging = false;
        }

        public void OnPointerEnter()
        {
            IsOver = true;
        }
        public void OnPointerExit() 
        {
            IsOver = false;
        }
        /*public void RandomSpeech()
        {
            //Debug.Log("Random Speech");
            if (!SpeechBubble.activeSelf) 
            { 
                int r = Random.Range(0,BonziDatalist.Count);            
                StartCoroutine(SpeakCo(r));
            }
        }*/
        public void FindBonziDataofName(string name ) 
        {
            BonziData SelectedBD = BonziDataFinderDic[name];
            StartCoroutine(SpeakCo(BonziDatalist.IndexOf(SelectedBD)));                
        }
        public void FindRandomBDofType(List<string> TypeList) 
        {
            if (!SpeechBubble.activeSelf)
            {
                int r = Random.Range(0, TypeList.Count);
                FindBonziDataofName(TypeList[r]);
            }
        }
        public void Download() 
        {  
            
            GameObject Download=Instantiate(DownloadMalus,new Vector3(0,0,this.transform.position.z),Quaternion.identity);
            Download.transform.parent = this.transform.parent;
            Download.transform.localScale = new Vector3(1,1,1);
            Destroy(Download,7f);
            
        }
        public void MoreBuddy() 
        { 
            if (MaxBuddy >= 0)
            {
                GameObject NewBonzi = Instantiate(BonziPrefab, new Vector3(0, 0, this.transform.position.z), Quaternion.identity);
                NewBonzi.transform.parent = this.transform.parent;
                NewBonzi.transform.localScale = new Vector3(1, 1, 1); 
                MaxBuddy--; 
            }
        }

        public void OnGameStart(int minigamecod)
        {
            
            string MinigameName = GameManager.Instance.ListaMinigiochi[minigamecod].prefab.name;
            FindBonziDataofName(MinigameName);
        
        }

        public void OnGameWin()
        {
            return;
        }

        public void OnGameLose()
        {
            return;
        }
    }
    
    
    [System.Serializable]
    public class BonziData
    {
        public string BonziDataName, BonziTextString,BonziAnimationTag;
        public AudioClip BonziSpeechClip;
    }
    
}
