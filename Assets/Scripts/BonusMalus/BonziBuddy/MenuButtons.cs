﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BonziBuddy
{
    public class MenuButtons : MonoBehaviour
    {
        public BonziBuddy_Controller BonziController;
       
        
        public void Speak() 
        {             
            BonziController.FindRandomBDofType(BonziController.BonziRandom);
        }
        public void Sing() 
        {             
            BonziController.FindRandomBDofType(BonziController.BonziSong);
        }
        public void Joke() { BonziController.FindRandomBDofType(BonziController.BonziJoke); }
        public void Facts() { BonziController.FindRandomBDofType(BonziController.BonziFacts); }
        public void Listen() { BonziController.FindRandomBDofType(BonziController.BonziListen); }
        public void AudioVideo() { BonziController.FindRandomBDofType(BonziController.BonziAudioVideo); }
        public void ReadStory() { BonziController.FindRandomBDofType(BonziController.BonziStory); }
        public void Download() { BonziController.Download(); }
        public void More() { BonziController.MoreBuddy(); }

    }
}
