﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSOD : State
{
    public float tempoLimite = 10f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Counter());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator Counter()
    {


        yield return new WaitForSeconds(tempoLimite);
        GameManager.Instance.RemoveState("BlueScreenOfDeath");

        yield return null;
    }
}
