﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firefox_controller : State
{
    public Canvas MainCanvas;
    public GameObject Fireball;
    public int fireballs = 5;

    private void Start()
    {
        StartCoroutine(Spawn_Fireball());
    }
    IEnumerator Spawn_Fireball()
    {
        for (int i = 0; i< fireballs; i++)
        {
            GameObject go = Instantiate(Fireball, this.gameObject.transform);
            go.gameObject.transform.SetParent(MainCanvas.transform);
            yield return new WaitForSecondsRealtime(3f);
        }
    } 
}
