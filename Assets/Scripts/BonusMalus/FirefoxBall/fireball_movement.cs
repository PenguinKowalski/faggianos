﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fireball_movement : State
{
    Canvas canvas;

    public int lifetime = 5;

    RectTransform rect;
    public Vector3 dir;
    public float speed = 1f;

    public float height, width;

    public float borderUp, borderDown, borderLeft, borderRight;

    public Vector2 buttonSize;

    public Vector2 rectPos;

    void Start()
    {
        canvas = this.gameObject.GetComponentInParent<Canvas>();

        rect = this.GetComponent<RectTransform>();

        dir = new Vector3(1f, Random.Range(-1f, 1f));

        height = canvas.GetComponent<RectTransform>().sizeDelta.y;
        width = canvas.GetComponent<RectTransform>().sizeDelta.x;

        buttonSize = rect.sizeDelta * 0.5f;
    }

    void Update()
    {
        FireballDeath();

        rect.position += speed * dir;
        rectPos = rect.anchoredPosition;

        borderDown = (canvas.transform.position + new Vector3(0, -height / 2f, 0)).y;
        borderUp = (canvas.transform.position + new Vector3(0, height / 2f, 0)).y;
        borderLeft = (canvas.transform.position + new Vector3(-width / 2f, 0, 0)).x;
        borderRight = (canvas.transform.position + new Vector3(+width / 2f, 0, 0)).x;

        Vector3 thescale = this.transform.localScale;
        if (rectPos.y + buttonSize.y > borderUp)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            lifetime--;
            thescale.y *= -1;
            this.transform.localScale = thescale;
        }
        if (rectPos.y - buttonSize.y < borderDown)
        {
            dir = new Vector3(dir.x, -dir.y, dir.z);
            lifetime--;
            thescale.y *= -1;
            this.transform.localScale = thescale;
        }
        if (rectPos.x + buttonSize.x > borderRight)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            lifetime--;
            thescale.x *= -1;
            this.transform.localScale = thescale;
        }
        if (rectPos.x - buttonSize.x < borderLeft)
        {
            dir = new Vector3(-dir.x, dir.y, dir.z);
            lifetime--;
            thescale.x *= -1;
            this.transform.localScale = thescale;
        }
    }

    public void FireballDeath()
    {
        if (lifetime == 0)
            Destroy(this.gameObject);
    }
}
