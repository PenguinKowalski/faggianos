﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TossACoin : State
{
    int donation = 100;
    int steal = 1000;
    public void Toss()
    {
        GameManager.Instance.ChangeMoney(-donation);
        StartCoroutine(CloseThis());
    }

    public void Mmmh()
    {
        GameManager.Instance.ChangeMoney(-steal);
        StartCoroutine(CloseThis());
    }

    IEnumerator CloseThis()
    {
        yield return new WaitForSecondsRealtime(2f);
        //Destroy(this.gameObject);
        this.gameObject.SetActive(false);
        yield return null;
    }
}
