﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelBreack : State
{
    float tempoLimite;

    void Start()
    {
        tempoLimite = 30f;
        StartCoroutine(Counter());
    }

    IEnumerator Counter()
    {
        int count = 0;
        while (count < tempoLimite)
        {
            count++;
            yield return new WaitForSeconds(1);
        }
        GameManager.Instance.RemoveState("ScreenBreak");

        yield return null;
    }
}
