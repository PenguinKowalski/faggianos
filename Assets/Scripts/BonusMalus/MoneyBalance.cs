﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyBalance : State
{
    public int minSoldi = 0;
    public int MaxSoldi = 100;
    // Start is called before the first frame update


    public override void Activate(string cod, StateData sd)
    {
        base.Activate(cod, sd);
        GameManager.Instance.ChangeMoney(minSoldi);
    }

}
