﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioMaria : State
{
    public AudioSource AudioSource;
    float tempoLimite;

    void Start()
    {
        int randomStartTime = Random.Range(0, AudioSource.clip.samples - 1);
        AudioSource.timeSamples = randomStartTime;
        AudioSource.Play();

        tempoLimite = 60f;
        StartCoroutine(Counter());
    }

    IEnumerator Counter()
    {
        int count = 0;
        while (count < tempoLimite)
        {
            count++;
            yield return new WaitForSeconds(1);
        }
        GameManager.Instance.RemoveState("RadioMaria");

        yield return null;
    }
}
