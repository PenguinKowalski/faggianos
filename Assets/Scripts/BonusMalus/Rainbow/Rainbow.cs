﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rainbow : State
{
    public float Lifespan;
    private float Timer;
    private void Start()
    {
        Timer = 0;
    }
    private void Update()
    {
        Timer += Time.deltaTime;
        if (Timer >= Lifespan)
            Destroy(this.gameObject);
    }
}

