﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : MonoBehaviour
{
    string codState;
    StateData statedata;

    public string CodState { get => codState; set => codState = value; }
    public StateData Statedata { get => statedata; set => statedata = value; }

    // Start is called before the first frame update
    public virtual void Activate(string cod, StateData sd)
    {
        CodState = cod;
        statedata = sd;

        if (statedata.iconDxIndex >= 0)
        {
            GameManager.Instance.listIconeStartDx[statedata.iconDxIndex].SetActive(true);
        }
    }

    public virtual void Deativate()
    {
        if (statedata.iconDxIndex >= 0)
        {
            GameManager.Instance.listIconeStartDx[statedata.iconDxIndex].SetActive(false);
        }
        Destroy(this.gameObject);
    }

    // Update is called once per frame
    public virtual void isStateAvailable()
    {
        
    }
}

[System.Serializable]
public class StateData
{
    public string Cod;
    public GameObject prefab;
    public int rarity;
    public int NumAttivi = 0;
    public int NumAttiviMax = 1;
    public int iconDxIndex = -1;
}
