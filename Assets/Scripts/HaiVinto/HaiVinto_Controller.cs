﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
public class HaiVinto_Controller : Minigame
{
    public GameObject HaiVinto, HaiPerso;
    public VideoPlayer Youlose_video;

    public double durata_video;

    public bool isLose = false;

    private void Start()
    {
        durata_video = Youlose_video.length;
    }

    private void Update()
    {
        if (isLose == true)
            WatchVideo();
    }

    public void WatchVideo()
    {
        if (durata_video >= 0f)
        {
            durata_video -= Time.deltaTime;
            if (durata_video <= 0f)
            {
                Lose();
            }
        }
    }

    public void YouLoseOnClick()
    {
        isLose = true;

        HaiVinto.SetActive(false);
        HaiPerso.SetActive(true);
    }

}
