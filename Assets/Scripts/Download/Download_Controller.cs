﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Download_Controller : State
{
    public Slider download_bar;
    public float fill_speed = 0.5f;
    private float target_progress = 0f;

    public bool isLose = false;
    public TextMeshProUGUI percent;

    private void Start()
    {
        IncrementProgress(100f);
        StartCoroutine(Losing());
    }

    void Update()
    {
        percent.text = ((download_bar.value * 100).ToString("f2") + "%");
        if (download_bar.value < target_progress)
            download_bar.value += fill_speed * Time.deltaTime;
    }   

    public void IncrementProgress(float new_progress)
    {
        target_progress = download_bar.value + new_progress;
    }

    public void CloseDownload()
    {
        GameManager.Instance.RemoveState("DownloadDaFermare");
    }

    IEnumerator Losing()
    {
        while (!isLose)
        {
            yield return new WaitForSecondsRealtime(1f);
        }

        if (isLose == true)
        {
            if (download_bar.value == 1)
            {
                GameManager.Instance.Punish(1f);
                GameManager.Instance.RemoveState("DownloadDaFermare");
            }
            yield return null;
        }
    }
}
