﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkipButton_Controller : Minigame
{
    public TextMeshProUGUI FakeTimerText;
    public int realTimer,fakeTimer;
    public bool isWinnable;

    void Start()
    {
        isWinnable = true;
        StartCoroutine(Countdown());
    }

    void Update()
    {
        FakeTimerText.text = "" + fakeTimer;

        if (realTimer == 0 && isWinnable == true)
            Victory();
    }

    IEnumerator Countdown()
    {
        while (true)
        {
            fakeTimer = Random.Range(realTimer, 99);
            realTimer--;
            yield return new WaitForSeconds(1f);
        }
    }

    public void LoseOnClick()
    {
        isWinnable = false;
        Lose();
    }
}
