﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IconaTab : MonoBehaviour
{
    public TextMeshProUGUI testo;
    public Image icona;
    public void Set(Sprite sprite, string text)
    {
        icona.sprite = sprite;
        testo.text = text;
    }

    
}
