﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class Finestra : MonoBehaviour
{
    string cod;

    [SerializeField]  public Transform content_full, content_half, toolbar_root, content_full_root, content_half_root;

    int nToolbars;
    public float heightBars;
 //   public GameObject debugContent;
    [SerializeField]
    bool isContentFull;

    public Transform Toolbar_root { get => toolbar_root; set => toolbar_root = value; }

    public float width_content, height_content;

    public Button closeButton;
    public TextMeshProUGUI TitleText, DetailText, TimerText;

    public GameObject prefabTab;
    public GameObject tab;

    public UnityEvent scrollClick;

    // Start is called before the first frame update
    void Start()
    {
       // InitContentOnWindow(debugContent);
    }
    public void InitContentOnWindow(string codeWindow, GameObject content, bool contentFull, bool isMain, int height=0)
    {
        cod= codeWindow;
        GameObject go;
        RectTransform rt_contentWindow;
        isContentFull = contentFull;

        DesktopController dc = FindObjectOfType<DesktopController>();

        tab = GameObject.Instantiate(prefabTab, dc.tabLayout);
      

        if (isContentFull)
        {
           go = GameObject.Instantiate(content, content_full);
            content_full_root.gameObject.SetActive(true);
            content_half_root.gameObject.SetActive(false);
           rt_contentWindow= content_full.GetComponent<RectTransform>();

        }
        else
        {
           go=  GameObject.Instantiate(content, content_half);
            content_full_root.gameObject.SetActive(false);
            content_half_root.gameObject.SetActive(true);
            rt_contentWindow = content_half.GetComponent<RectTransform>();

        }
        Minigame mg = this.GetComponentInChildren<Minigame>();
        mg.finestraRef = this;
        TitleText.text = mg.Title;
        DetailText.text = mg.Detail;
        mg.isMain = isMain;
        tab.GetComponent<IconaTab>().Set( mg.icona, mg.Title);
        mg.SpawnFinestra();

        RectTransform rt= go.GetComponent<RectTransform>();

        if (rt == null)
        {
            rt = rt_contentWindow.transform.GetChild(1).GetComponent<RectTransform>() ; 
        }
        Vector2 size = rt.sizeDelta;

        width_content = size.x;

        RectTransform rt_root = this.GetComponent<RectTransform>();
        RectTransform rt_viewport = rt_contentWindow.parent.GetComponent<RectTransform>();

        rt_root.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);


        height_content = size.y;
        if (height != 0)
            height_content = height;
        

        rt_root.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);

        rt_contentWindow.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
        rt_contentWindow.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
        rt_contentWindow.anchoredPosition = new Vector2(-rt_contentWindow.sizeDelta.x/2f, 0);

        rt_root.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height_content + heightBars);



    }

    public void Close(int closewinlose)
    {
        GameManager.Instance.CloseWindow(cod, closewinlose);

    }
    public void SpawnToolbarElements(GameObject parent)
    {



    }

    public virtual void OnScrollChange()
    {
        scrollClick.Invoke();
    }
    private void OnDestroy()
    {
        if (tab)
        Destroy(tab);
    }
}
