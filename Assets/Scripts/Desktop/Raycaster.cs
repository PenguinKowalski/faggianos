﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VegetableCrush;

public interface IVirtualClick
{
     void OnMouseClickDown(Vector2 clickpos);
    void OnMouseClickUp(Vector2 clickpos);
}
public class Raycaster : MonoBehaviour
{
    [SerializeField]
     List<RaycastResult> results = new List<RaycastResult>();
    VirtualScreen gr;
    //Create the PointerEventData with null for the EventSystem
    PointerEventData ped = new PointerEventData(null);

    public RawImage image;

    public Camera mainCamera;
    int texheight, texwidth;

    private void Start()
    {
        gr = this.GetComponent<VirtualScreen>();
        mainCamera = Camera.main;
        texheight = image.mainTexture.height;
        texwidth = image.mainTexture.width;
    }

    public void Update()
    {
        //Set required parameters, in this case, mouse position
        ped.position = VirtualCursor.instance.CursorPosition;
        //Create list to receive all results
        results.Clear();
        //Raycast it
        gr.Raycast(ped, results);

        // foreach (RaycastResult rr in results)
        
       if (results.Count>0)
        {
          //  Debug.Log(results[0].gameObject.name);
            if (results[0].gameObject == image.gameObject)
            {
                //gr.RaycastBeyondTV(results[0].screenPosition , results);
               // Debug.Log(results[0].screenPosition);
                Rect r = image.rectTransform.rect;
                Vector2 localPoint;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(image.rectTransform, ped.position, mainCamera, out localPoint);
                int px = Mathf.Clamp(0, (int)(((localPoint.x - r.x) * texwidth) / r.width), texwidth);
                int py = Mathf.Clamp(0, (int)(((localPoint.y - r.y) * texheight) / r.height), texheight);

                Vector2 pix = new Vector2(px, py);
             //   Debug.Log(new Vector2(px,py));
                gr.RaycastBeyondTV(pix, results);


                if (Input.GetMouseButtonDown(0))
                {
                    if (gr.lastSelected != null)
                    {
                        IVirtualClick iclick = gr.lastSelected.GetComponent<IVirtualClick>();
                        if (iclick!=null)                           
                           iclick.OnMouseClickDown(gr.ray.origin);
                    }

                }
                /*
                if (Input.GetMouseButtonUp(0))
                {
                    if (gr.lastSelected != null)
                    {
                        gr.lastSelected.GetComponent<IVirtualClick>().OnMouseClickUp(gr.ray.origin);
                    }

                }*/

            }
        }
            

    }


}
