﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartBar : MonoBehaviour
{

    RectTransform rect;
    public Rect rect_screenspace;
   
    public Vector2 localMousePosition;
    private void Start()
    {
        rect = this.GetComponent<RectTransform>();
        rect_screenspace= rect.rect; // = VirtualCursor.RectTransformToScreenSpace(rect);
    }
    // Update is called once per frame
    void Update()
    {
       // Debug.Log(EventSystem.current.currentSelectedGameObject);

        if (Input.GetMouseButtonDown(0))
        {
            localMousePosition = rect.InverseTransformPoint(VirtualCursor.instance.cursor.transform.position);

            if (!rect_screenspace.Contains(localMousePosition))
            {
                this.gameObject.SetActive(false);
            }

        }

              
    }


}
