﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum WindowState {Account, Login, Screensaver, Desktop }

public class DesktopController : MonoBehaviour
{
    public Database db;
    // Start is called before the first frame update
    public GameObject Desktop, ScreenSaver, Account, Login, Antivirus;

    public WindowState stato = WindowState.Account;

    public TMP_InputField loginIF, passwordIF;
    int nPassword = 9;

    public Image[] imageAccounts;
    public TextMeshProUGUI[] nameAccounts;

    public CanvasGroup desktopCanvas, screensaverCanvas;

    public GameObject startBar;

    public Transform tabLayout;

    public TextMeshProUGUI TimeText, ZucchiniText, Monetetext;

    public bool FASTLOAD = false;

    //malus desktop che cambia sfondo
    public int indicesfondo;
    public float aspettasfondo;
    public GameObject sfondoverde;
    public Image sfondostandard;
    public List<Sprite> Sfondi = new List<Sprite>();
    //malus desktop che cambia sfondo
    void Awake()
    {
#if !UNITY_EDITOR
        FASTLOAD=false;
#endif
        // db = FindObjectOfType<Database>();
        Desktop.SetActive(false);
        ScreenSaver.SetActive(false);
        Login.SetActive(false);
        Account.SetActive(true);
        desktopCanvas.alpha = 0;
        screensaverCanvas.alpha = 1;

        for (int i = 0; i < 4; i++)
        {
            if (imageAccounts[i].sprite != null)
            imageAccounts[i].sprite = db.accounts[i].immagine;
            nameAccounts[i].text = db.accounts[i].name;
        }

        if (FASTLOAD)
        {
            db.ActualSlot = 0;
            SetDesktopActive();
        }

        
            
    }
    private void Start()
    {
        if (GameManager.Instance.database.GameOverFlag)
        {
            Debug.Log("Il GameOverFlag è settato TRUE");
            GameManager.Instance.database.GameOverFlag = false;

        }
    }

    private void Update()
    {
        if (stato== WindowState.Login)
        {
            if (Input.anyKeyDown)
            {
                if (passwordIF.text.Length<nPassword)
                    passwordIF.text += "*";             
            }
        }

        TimeText.text = System.DateTime.Now.ToString("HH:mm");

       
    }

    public void LoginButtonOnClick()
    {
        if (passwordIF.text.Length >= nPassword)
            GoToScreen(WindowState.Screensaver);
    }
    void GoToScreen(WindowState _stato)
    {
        stato = _stato;
        if (stato == WindowState.Account)
        {
            
        }
        else if (stato == WindowState.Login)
        {
            Account.SetActive(false);
            Login.SetActive(true);
        }
        else if(stato == WindowState.Screensaver)
        {
            Login.SetActive(false);
            ScreenSaver.SetActive(true);
            Desktop.SetActive(true);
            LeanTween.alphaCanvas(desktopCanvas, 1, 1f).setDelay(5f);
            LeanTween.alphaCanvas(screensaverCanvas, 0, 1f).setDelay(5f).setOnComplete(SetDesktopActive);
        }
        else
        {
            Desktop.SetActive(true);
            ScreenSaver.SetActive(false);
            Login.SetActive(false);
            Account.SetActive(false);
            desktopCanvas.alpha = 1;
            Antivirus_Manager.instance.CheckSpawn();
        }

    }
    public void PauseToggleOnClick()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else Time.timeScale = 0;

    }
    public void RestartOnClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void QuitOnClick()
    {
        Application.Quit();
    }
    public void SetDesktopActive()
    {
        ScreenSaver.SetActive(false);
        GoToScreen(WindowState.Desktop);
        UpdateMoney();

    }
    public void SelectAccount(int slotNum)
    {
        db.ActualSlot = slotNum;
        loginIF.text = db.accounts[db.ActualSlot].name;
        GoToScreen(WindowState.Login);
    }

    public void StartButtonOnClick()
    {
        if (stato != WindowState.Desktop) return;

        startBar.SetActive(true);
    }

    public void OpenOutlook()
    {
        // outlook.SetActive(true);
        GameManager.Instance.OpenOutlook();
        startBar.SetActive(false);
    }

    public void OpenAntivirus()
    {
        Antivirus.GetComponent<CanvasGroup>().alpha = 1f;
        Antivirus.GetComponent<CanvasGroup>().interactable = true;
        Antivirus.GetComponent<CanvasGroup>().blocksRaycasts = true;
        startBar.SetActive(false);
    }

    public void UpdateMoney()
    {
        Monetetext.text = GameManager.Instance.Soldi.ToString();
        ZucchiniText.text = GameManager.Instance.database.GetZucchini().ToString();
    }
    //malus desktop che cambia sfondo
    IEnumerator CambiaSfondo() 
    {
        while (true) 
        {
            sfondoverde.SetActive(false);
            indicesfondo = Random.Range(0, Sfondi.Count);
            sfondostandard.sprite = Sfondi[indicesfondo];
            aspettasfondo = 30f;
            yield return new WaitForSeconds(aspettasfondo);
        }
        
    }
    public void AttivaSfondi() 
    {
        StartCoroutine(CambiaSfondo());
    }
    //malus desktop che cambia sfondo

}

