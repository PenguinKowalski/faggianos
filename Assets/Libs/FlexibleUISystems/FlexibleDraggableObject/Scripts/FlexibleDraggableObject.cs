﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class FlexibleDraggableObject : MonoBehaviour
{
    public GameObject Target;
    private EventTrigger _eventTrigger;
    public Transform parent_root;

    public Vector3 pos;
    void Start ()
    {
        if (!Target)
        {
            Target = this.gameObject;
        }
        _eventTrigger = GetComponent<EventTrigger>();
        _eventTrigger.AddEventTrigger(OnDrag, EventTriggerType.Drag);
        _eventTrigger.AddEventTrigger(OnDragStart, EventTriggerType.BeginDrag);


    }


    void OnDragStart(BaseEventData data)
    {
       //Debug.Log("dragStart");
        pos = VirtualCursor.instance.transform.position;
        
        if(!parent_root)
        {
            transform.SetAsLastSibling();
        }
        else
        {
            parent_root.SetAsLastSibling();
        }

    }
    void OnDrag(BaseEventData data)
    {
        PointerEventData ped = (PointerEventData) data;

        Vector3 allowedPosition = VirtualCursor.instance.transform.position;
        
            //Mathf.Clamp(VirtualCursor.instance.transform.position.x, GameManager.Instance.cameraRect.xMin, GameManager.Instance.cameraRect.xMax),
            // Mathf.Clamp(VirtualCursor.instance.transform.position.y, GameManager.Instance.cameraRect.yMin, GameManager.Instance.cameraRect.yMax),
   


        Vector3 delta = allowedPosition - pos;
        //Debug.Log(VirtualCursor.instance.transform.position);

        if (VirtualCursor.instance.transform.position.x > GameManager.Instance.safeArea.xMin &&
            VirtualCursor.instance.transform.position.x < GameManager.Instance.safeArea.xMax &&
            VirtualCursor.instance.transform.position.y < GameManager.Instance.safeArea.yMax  &&
            VirtualCursor.instance.transform.position.y > GameManager.Instance.safeArea.yMin)
        {
            Target.transform.Translate(delta);
            pos = VirtualCursor.instance.transform.position;
        }


    }
}